/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import apptsystemtjacqu2.model.Appointments;
import static apptsystemtjacqu2.model.Appointments.getApptList;
import apptsystemtjacqu2.model.DBAccess;
import static apptsystemtjacqu2.model.DBAccess.getPassword;
import apptsystemtjacqu2.model.LogIn;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class LogInFXMLController implements Initializable{
    
    @FXML
    private Button btnLogIn;
    
    @FXML
    private Button btnCancelLogIn;

    @FXML
    private TextField userNameField;

    @FXML
    private TextField passwordField;
    
    @FXML
    private Label userNameLabel;

    @FXML
    private Label passwordLabel;
    
    @FXML
    private Label lblLogInMsg;

    private AppointmentSystemMain mainApp;
    private String alertTitle;
    private String alertHeader;
    private String alertText;
    private String alertTitle2;
    private String alertHeader2;
    private String alertText2;
    private static boolean initFlag = true;
    public static String logInTitle;
    public static String popUserName;
    private static ObservableList<Appointments> apptList15min;
  
    public LogInFXMLController() {
        
    }
    
 @Override
    public void initialize(URL url, ResourceBundle rb) {
            setLanguageLabels();
    }

    @FXML
    private void handleBtnLogIn() throws Exception {
        String dbPassword;
        String timestamp;
        try{
            if (userNameField.getText().isEmpty() || passwordField.getText().isEmpty()) {
                throw new NullPointerException();
            }else{                
                dbPassword = DBAccess.checkApplicationCredentials(getPassword, "password", getUserNameField().getText());//SQLException
                if (LogIn.checkPassword(passwordField.getText(), dbPassword)){
                    System.out.println("UserName and password good, logging in");
                    popUserName = getUserNameField().getText();
                    checkApptsWithin15();
                    mainApp.showMainCalenderLayout();
                    timestamp = LocalDateTime.now().toString();
                    logInTimestamp(timestamp, getUserNameField().getText());//IOException
                }else{
                    //Invalid username or password
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle(alertTitle);
                    alert.setHeaderText(alertHeader);
                    alert.setContentText(alertText);
                    alert.showAndWait();
                    
                }
            }       
        }catch(NullPointerException npe) {
            //username and password cannot be blank
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle(alertTitle);
            alert.setHeaderText(alertHeader2);
            alert.setContentText(alertText2);
            alert.showAndWait();
        }
    }
    
    @FXML
    private void handleBtnCancelLogIn() throws SQLException{
        DBAccess.dbConnectionClose(DBAccess.ps, DBAccess.dbConnection);
        System.exit(0);    
    }
    
    public void setMainApp(AppointmentSystemMain mainApp) {
        this.mainApp = mainApp;
    }
    
    private void logInTimestamp(String LItimestamp, String user) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("../apptsystemtjacqu2/src/apptsystemtjacqu2/controller/LogInTracker.txt", true))) {
            writer.append(user+ " " +LItimestamp);
            writer.newLine();
        }catch(IOException ex){
           System.out.println(ex.getMessage());
        }
        
    }
    
    private void checkApptsWithin15() throws SQLException, ParseException {
        DBAccess.getConsultantScheduleData(getUserNameField().getText());
        LocalTime currentTime = LocalTime.now();
        for(Appointments a : getApptList()) {
            String input = LocalDate.now().toString();
            SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat outputDate = new SimpleDateFormat("MM-dd-yyyy");
            Date date = inputDate.parse(input);
            String date2 = outputDate.format(date);
            String tsMilitary = a.getTimeStart().toString().substring(11);
            LocalTime tsMilAppt = LocalTime.parse(tsMilitary);
            
            if(a.getDate().equals(date2) && tsMilAppt.isAfter(currentTime) && tsMilAppt.isBefore(currentTime.plusMinutes(16))) {
                
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Reminder");
                alert.setHeaderText("Appointment Reminder");
                alert.setContentText("You have an Appointment with " + a.getContact() + " of " + a.getCustomer() + " at " + a.getTimeStartSt());
                alert.showAndWait();
            }
        }
    }
    
    public void setLanguageLabels() {
        //Locale.setDefault(new Locale("fr", "FR"));
        Locale.setDefault(new Locale("en", "US"));
        ResourceBundle rb = ResourceBundle.getBundle("apptsystemtjacqu2.controller.Bundle", Locale.getDefault());
        userNameLabel.setText(rb.getString("lblUserName"));
        passwordLabel.setText(rb.getString("lblPassword"));
        btnLogIn.setText(rb.getString("btnLogIn"));
        btnCancelLogIn.setText(rb.getString("btnCancel"));
        alertTitle = rb.getString("alertTitle");
        alertHeader = rb.getString("alertHeader");
        alertText = rb.getString("alertText");
        alertTitle2 = rb.getString("alertTitle2");
        alertHeader2 = rb.getString("alertHeader2");
        alertText2 = rb.getString("alertText2");
        logInTitle = rb.getString("logInTitle");
    }

    /**
     * @return the userNameField
     */
    public TextField getUserNameField() {
        return userNameField;
    }

    
}
