/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import apptsystemtjacqu2.model.Appointments;
import apptsystemtjacqu2.model.DBAccess;
import static apptsystemtjacqu2.model.DBAccess.dbConnection;
import static apptsystemtjacqu2.model.DBAccess.dbConnectionClose;
import static apptsystemtjacqu2.model.DBAccess.ps;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class GenerateReportsFXMLController implements Initializable {
    private AppointmentSystemMain mainApp;
    private Stage secondaryStage;
    public static ObservableList<Appointments> apptSchedule;
    private enum Type {TYPEBYMONTH, CONSULTANT, MISC}
    private Type type;
    @FXML
    private void handleReportBtnApptTypes() {
        type = Type.TYPEBYMONTH;
        apptSchedule = Appointments.getApptList();
        Collections.sort(apptSchedule,(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
        try {
            File file = new File("../apptsystemtjacqu2/src/apptsystemtjacqu2/controller/ReportTypeByMonth.txt");
            writeDataToTextFile(apptSchedule, file);
            openFile(" ");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @FXML
    private void handleReportBtnConsultantSchedule() {
        type = Type.CONSULTANT;
        apptSchedule = Appointments.getApptList();
        Collections.sort(apptSchedule,(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
        try{
            File file = new File("../apptsystemtjacqu2/src/apptsystemtjacqu2/controller/ConsultantSchedule.txt");
            writeDataToTextFile(apptSchedule, file);
            openFile(" ");
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
    
    @FXML
    private void handleBtnReportMisc() {
        type = Type.MISC;
        apptSchedule = Appointments.getApptList();
        Collections.sort(apptSchedule,(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
        try {
            File file = new File("../apptsystemtjacqu2/src/apptsystemtjacqu2/controller/TotalApptTypesByMonth.txt");
            writeDataToTextFile(apptSchedule, file);
            openFile(" ");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @FXML
    private void handleBtnCancel() {
        secondaryStage.close();
    }
    
    private void writeDataToTextFile(ObservableList<Appointments> list, File file) throws IOException{
        boolean firstCheck = true;
        String tempMonth = "";
        int incSales = 0;
        int incFollowUp = 0;
        int incContractClose = 0;
        int incCustomerSupport = 0;
        int incTotal = 0;
        switch(type) {
            case TYPEBYMONTH:
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                writer.write("Appointment Types by Month");
                writer.newLine();
                writer.newLine();
                for(Appointments a : list){
                    SimpleDateFormat monthConvert = new SimpleDateFormat ("MMMM");
                    String monthFormat = monthConvert.format((java.util.Date) a.getTimeStart());
                    if(firstCheck) {
                       tempMonth = monthFormat;
                       firstCheck = false;
                    }
                    if (tempMonth.equals(monthFormat)) {
                        
                        if(a.getApptType().equals("Sales")){incSales++;}
                        if(a.getApptType().equals("Follow Up")){incFollowUp++;}
                        if(a.getApptType().equals("Contract Close")){incContractClose++;}
                        if(a.getApptType().equals("Customer Support")){incCustomerSupport++;}
                    }else{
                        writer.write(tempMonth);
                        writer.newLine();
                        if(incSales > 0) {
                        writer.write("Sales: " + incSales);
                        writer.newLine();
                        }
                        if(incFollowUp > 0){
                        writer.write("Follow Up: " + incFollowUp);
                        writer.newLine();
                        }
                        if(incContractClose > 0){
                        writer.write("Contract Close: " + incContractClose);
                        writer.newLine();
                        }
                        if(incCustomerSupport > 0){
                        writer.write("Customer Support: " + incCustomerSupport);
                        writer.newLine();
                        }
                        writer.newLine();
                        incSales = 0;
                        incFollowUp = 0;
                        incContractClose = 0;
                        incCustomerSupport = 0;
                        if(a.getApptType().equals("Sales")){incSales++;}
                        if(a.getApptType().equals("Follow Up")){incFollowUp++;}
                        if(a.getApptType().equals("Contract Close")){incContractClose++;}
                        if(a.getApptType().equals("Customer Support")){incCustomerSupport++;}
                        tempMonth = monthFormat;
                    }                    
                }
                writer.write(tempMonth);
                        writer.newLine();
                        if(incSales > 0){
                        writer.write("Sales: " + incSales);
                        writer.newLine();
                        }
                        if(incFollowUp > 0){
                        writer.write("Follow Up: " + incFollowUp);
                        writer.newLine();
                        }
                        if(incContractClose > 0){
                        writer.write("Contract Close: " + incContractClose);
                        writer.newLine();
                        }
                        if(incCustomerSupport > 0){
                        writer.write("Customer Support: " + incCustomerSupport);
                        writer.newLine();
                        }
                        writer.newLine();
                }catch(IOException ex) {
                    ex.printStackTrace();
                }
            break;
            case CONSULTANT:
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                    writer.write("Consultant Schedule");
                    writer.newLine();
                    for(Appointments a : list){
                        //writer.write(a.getConsultant());
                        //writer.newLine();
                        writer.write(a.getTimeStart() + ", " + a.getTimeEnd() + ", " + a.getApptType() + ", " + a.getCustomer()
                        + ", " + a.getPhone());
                        writer.newLine();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            break;
            case MISC:
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                    writer.write("Consultant Total Appointments by Month");
                    writer.newLine();
                    for(Appointments a : list){
                        SimpleDateFormat monthConvert = new SimpleDateFormat ("MMMM");
                        String monthFormat = monthConvert.format((java.util.Date) a.getTimeStart());
                        if(firstCheck) {
                           tempMonth = monthFormat;
                           firstCheck = false;
                        }
                        if (tempMonth.equals(monthFormat)) {
                            
                            if(a.getApptType().equals("Sales")){incTotal++;}
                            if(a.getApptType().equals("Follow Up")){incTotal++;}
                            if(a.getApptType().equals("Contract Close")){incTotal++;}
                            if(a.getApptType().equals("Customer Support")){incTotal++;}
                        }else{
                            writer.write(tempMonth);
                            writer.newLine();
                            writer.write("Total Appointments: " + incTotal);
                            writer.newLine();
                            incTotal = 0;
                            if(a.getApptType().equals("Sales")){incTotal++;}
                            if(a.getApptType().equals("Follow Up")){incTotal++;}
                            if(a.getApptType().equals("Contract Close")){incTotal++;}
                            if(a.getApptType().equals("Customer Support")){incTotal++;}
                            tempMonth = monthFormat;
                        }                    
                    }
                        writer.write(tempMonth);
                        writer.newLine();
                        writer.write("Total Appointments: " + incTotal);
                        writer.newLine();
                    
                }catch(IOException ex) {
                    ex.printStackTrace();
                }
            break;
        }
    }
    
    private void openFile(String file) {
        switch(type) {
            case TYPEBYMONTH:
                mainApp.showTypeByMonthReport();
                break;
            case CONSULTANT:
                mainApp.showConsultantReport();
                break;
            case MISC:
                mainApp.showTotApptsByMonthReport();
                break;
        }
    }
    
    
    
    
            
    
    public void setMainApp(AppointmentSystemMain mainApp) {
        this.mainApp = mainApp;
    }
    
    
    public void setSecondaryStage(Stage secondaryStage) {
		this.secondaryStage = secondaryStage;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            DBAccess.getConsultantScheduleData(LogInFXMLController.popUserName);
        } catch (SQLException ex) {
            try {
                dbConnectionClose(ps, dbConnection);
            } catch (SQLException ex1) {
                System.out.println(ex1.getMessage());
            }
            finally{
            System.out.println("Database Closed!");
            Alert alert = new Alert(Alert.AlertType.WARNING);
                 alert.setTitle("Alert");
                 alert.setHeaderText("Possible database connection issue");
                 alert.setContentText("Check Database Connection");
                 alert.showAndWait();
            //System.out.println("getConsultantScheduleData" + e.getMessage());
            }
        }
    }    
    
}