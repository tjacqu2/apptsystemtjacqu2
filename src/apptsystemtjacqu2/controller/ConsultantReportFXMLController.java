/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import apptsystemtjacqu2.model.Appointments;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class ConsultantReportFXMLController implements Initializable {
    private Stage secondaryStage;
    private AppointmentSystemMain mainApp;
    
    @FXML
    private TableView<Appointments> listViewReports;
    
    @FXML
    private TableColumn<Appointments, String> dateColumn;

    @FXML
    private TableColumn<Appointments, String> consultantColumn;

    @FXML
    private TableColumn<Appointments, String> apptStartColumn;

    @FXML
    private TableColumn<Appointments, String> apptEndColumn;

    @FXML
    private TableColumn<Appointments, String> apptTypeColumn;

    @FXML
    private TableColumn<Appointments, String> customerColumn;

    @FXML
    private TableColumn<Appointments, String> phoneColumn;
    
    @FXML 
    private Label lblConsultant;
    
    private void handleBtnPrint() {
        
    }
    
    @FXML
    private void handleBtnCancel() {
       secondaryStage.close(); 
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblConsultant.setText("Consultant: " + LogInFXMLController.popUserName);
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
        apptStartColumn.setCellValueFactory(cellData -> cellData.getValue().timeStartPropertySt());
        apptEndColumn.setCellValueFactory(cellData -> cellData.getValue().timeEndStProperty());
        apptTypeColumn.setCellValueFactory(cellData -> cellData.getValue().apptTypeProperty());
        customerColumn.setCellValueFactory(cellData -> cellData.getValue().customerProperty());
        phoneColumn.setCellValueFactory(cellData -> cellData.getValue().phoneProperty());
        
        
        
    }    

    public void setSecondaryStage(Stage secondaryStage) {
        this.secondaryStage = secondaryStage;
    }

    public void setMainApp(AppointmentSystemMain mainApp) {
        this.mainApp = mainApp;
        listViewReports.setItems(GenerateReportsFXMLController.apptSchedule);
    }
    
}
