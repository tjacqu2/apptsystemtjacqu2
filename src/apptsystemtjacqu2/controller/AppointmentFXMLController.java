/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import static apptsystemtjacqu2.controller.CustomerModFXMLController.reload;
import static apptsystemtjacqu2.controller.MainScreenCalenderFXMLController.month;
import apptsystemtjacqu2.model.Appointments;
import static apptsystemtjacqu2.model.Appointments.getApptList;
import static apptsystemtjacqu2.model.Appointments.getApptListMonthly;
import static apptsystemtjacqu2.model.Appointments.getApptListWeekly;
import apptsystemtjacqu2.model.Customer;
import static apptsystemtjacqu2.model.Customer.getCustomerList;
import apptsystemtjacqu2.model.DBAccess;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class AppointmentFXMLController implements Initializable{
//public class AppointmentFXMLController {    
    private ObservableList<String> apptTimeList = FXCollections.observableArrayList(
            "08:00 AM", "08:15 AM", "08:30 AM", "08:45 AM", "09:00 AM", "09:15 AM", "09:30 AM", "09:45 AM", "10:00 AM", "10:15 AM", "10:30 AM", 
            "10:45 AM", "11:00 AM", "11:15 AM", "11:30 AM", "11:45 AM", "12:00 PM", "12:15 PM", "12:30 PM", "12:45 PM", "01:00 PM", "01:15 PM",
            "01:30 PM", "01:45 PM", "02:00 PM", "02:15 PM", "02:30 PM", "02:45 PM", "03:00 PM", "03:15 PM", "03:30 PM", "03:45 PM", "04:00 PM", 
            "04:15 PM", "04:30 PM", "04:45 PM", "05:00 PM");
    private ObservableList<String> apptTypeList = FXCollections.observableArrayList("Sales", "Follow Up", "Contract Close", "Customer Support");
    
    @FXML
    private TextField custIDField;
    
    @FXML
    private TextField custNameField;
    
    @FXML
    private TextField custContactField;
    
    @FXML
    private TextField custTitleField;
    
    @FXML
    private ChoiceBox<String> apptTimeStBox;
    
    @FXML
    private ChoiceBox<String> apptTimeEndBox;
    
    @FXML
    private ChoiceBox<String> apptTypeBox;

    @FXML
    private DatePicker apptDateField;

    @FXML
    private TextField custPhoneField;

    @FXML
    private TableView<Customer> custTable;
    
    @FXML
    private TableColumn<Customer, Integer> idColumn;

    @FXML
    private TableColumn<Customer, String> nameColumn;

    @FXML
    private TableColumn<Customer, String> phoneColumn;

    @FXML
    private TextField searchCustTextField;
    
    private AppointmentSystemMain mainApp;
    private Stage secondaryStage;
    private static ObservableList<Appointments> apptScheduleAll;
       

    /*private void handleCustSearch() { 
        String searchText = searchCustTextField.getText();
        FilteredList<Customer> searchCustResults = searchCustomers(searchText);
        SortedList<Customer> sortedData = new SortedList<>(searchCustResults);
        sortedData.comparatorProperty().bind(custTable.comparatorProperty());
        custTable.setItems(sortedData);
        if (searchCustResults.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Customer Not Found");
            alert.setContentText("No customer was found. Re-try search");
            alert.showAndWait();
        }
    }

    /**
     * searches part by part ID or name
     * @param s search string
     * @return part match
     */
    /*private FilteredList<Customer> searchCustomers(String cust){
        
           return Customer.getCustomerList().filtered(c -> c.getCustName().contains(cust)); 
        
    }*/
    /**
     * searches part by part ID or name
     * @param s search string
     * @return part match
     */

    private void setIdNamePhone() {
        Customer Cust = custTable.getSelectionModel().getSelectedItem();
        custNameField.setText(Cust.getCustName());
        custPhoneField.setText(Cust.getCustPhone());
        custIDField.setText(Integer.toString(Cust.getCustID()));
    }
    
    
    @FXML
    private void handleBtnApptSave() {
        boolean save = true;
        if(!custIDField.getText().isEmpty() && !custNameField.getText().isEmpty()
                && !custPhoneField.getText().isEmpty() && !custContactField.getText().isEmpty() 
                && !custTitleField.getText().isEmpty() && apptDateField.getValue() != null 
                && apptTimeStBox.getValue() != null && apptTimeEndBox != null && apptTypeBox.getValue() != null) {
            try{//check for end of appointment before start of appointment
                LocalTime timeChkStart = DBAccess.timeStringToLocalTimeMilitary(apptTimeStBox.getValue());
                LocalTime timeChkEnd = DBAccess.timeStringToLocalTimeMilitary(apptTimeEndBox.getValue());
                if(timeChkEnd.minusMinutes(1).isBefore(timeChkStart)){
                    throw new Exception();
                }
                try{//check for appointment conflicts
                    if(checkConflictingAppointments()) {
                        throw new Exception();
                    }
                    try{
                        String date = apptDateField.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));//formatted for timestamp
                        DBAccess.addAppointment(custNameField.getText(), custContactField.getText(), custTitleField.getText(), date, custIDField.getText(), apptTypeBox.getValue(), 
                                apptTimeStBox.getValue(), apptTimeEndBox.getValue(), LogInFXMLController.popUserName, LogInFXMLController.popUserName);
                        reload = false;
                        DBAccess.getDBCustomerList();
                        Collections.sort(getCustomerList(),(m1, m2) -> m1.getCustName().compareTo(m2.getCustName()));
                        if(!month){
                        DBAccess.getConsultantScheduleDataWeekly(LogInFXMLController.popUserName, MainScreenCalenderFXMLController.newWeek);
                        Collections.sort(getApptListWeekly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
                        }
                        else{
                        DBAccess.getConsultantScheduleDataMonthly(LogInFXMLController.popUserName, MainScreenCalenderFXMLController.newMonth, MainScreenCalenderFXMLController.newYear);
                        Collections.sort(getApptListMonthly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
                        }
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Alert");
                        alert.setHeaderText("Appointment Added");
                        alert.setContentText("Would you like to make another appointment?");
                        ButtonType button1 = new ButtonType("Yes");
                        ButtonType button2 = new ButtonType("No", ButtonData.NO);
                        alert.getButtonTypes().setAll(button1, button2);
                        Optional<ButtonType> result = alert.showAndWait();
                        
                        result.filter(response -> result.get() == button2)
                        .ifPresent(response -> secondaryStage.close());
                        clearFields();
                        reload = true;
                    }catch(SQLException e){                    
                        System.out.println("Appointment Save " + e.getMessage());
                    }
                }catch(Exception e){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Alert");
                    alert.setHeaderText("Conflicting Appointment in This Time Slot");
                    alert.setContentText("Please change appointment time");
                    alert.showAndWait();
                }
            }catch(Exception e){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Alert");
                alert.setHeaderText("Start time cannot be after or equal to end time");
                alert.setContentText("Please change appointment times");
                alert.showAndWait(); 
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("All Fields Are Not Populated");
            alert.setContentText("Please populate all fields");
            alert.showAndWait();
        }
    }
    
    
    private boolean checkConflictingAppointments() throws SQLException, ParseException {
        boolean conflict = false;
        
        DBAccess.getConsultantScheduleDataAll();
        ObservableList<Appointments> list = getApptList();
        for(Appointments a : list) {
            
            String stConcat = apptTimeStBox.getValue().substring(0, 5);
            String endConcat = apptTimeEndBox.getValue().substring(0, 5);
            LocalTime apptSt = LocalTime.parse(stConcat);//string to LocalTime
            LocalTime apptEnd = LocalTime.parse(endConcat);//string to LocalTime
            LocalTime dbStartTi = LocalTime.parse(a.getTimeStartSt().substring(0, 5));//string to LocalTime
            LocalTime dbEndTi = LocalTime.parse(a.getTimeEndSt().substring(0, 5));//string to LocalTime
     
            String outDateStr = apptDateField.getValue().format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
            //are there any appointments on that date?
            if (a.getDate().equals(outDateStr)) {//a.getDate s/b MM-dd-yyyy, was dd-MM-yyyy
            //Does this consultant have any appointments with any customer in the prospective timeframe?
            //match user here
                if (a.getConsultant().equals(LogInFXMLController.popUserName)) {
                    if((apptSt.plusMinutes(1).isAfter(dbStartTi) && apptSt.isBefore(dbEndTi))//minusMinutes(1)
                            || (apptEnd.isAfter(dbStartTi) && apptEnd.minusMinutes(1).isBefore(dbEndTi))){//.plusMinutes(1)
                    conflict = true;
                    System.out.println("Current consultant has conflicting appointment");
                    }
                }else if(a.getCustomerId() == Integer.parseInt(custIDField.getText())){
                //Does this specific customer have any appointments with any consultant for this appointment timeframe?    
                    if((apptSt.plusMinutes(1).isAfter(dbStartTi) && apptSt.isBefore(dbEndTi))
                            || (apptEnd.isAfter(dbStartTi) && apptEnd.minusMinutes(1).isBefore(dbEndTi))){
                    conflict = true;
                    System.out.println("Another Consultant has conflicting appointment with same customer");
                    }
                }                
            }           
        }
        return conflict;
    }
    
    private void clearFields() {
       custNameField.clear();
       custContactField.clear();
       custPhoneField.clear();
       custTitleField.clear();
       custIDField.clear();
       apptDateField.setValue(null);
       apptTypeBox.setValue(null); 
       apptTimeStBox.setValue(null); 
       apptTimeEndBox.setValue(null);
             
    }
    @FXML
    private void handleBtnApptCancel() {
        reload = true;
        secondaryStage.close();
    }
    
    public void setMainApp(AppointmentSystemMain mainApp) {
            this.mainApp = mainApp;
            Collections.sort(getCustomerList(),(m1, m2) -> m1.getCustName().compareTo(m2.getCustName()));
            custTable.setItems(Customer.getCustomerList());
    }
    
    public void setSecondaryStage(Stage secondaryStage) {
		this.secondaryStage = secondaryStage;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        reload = true;
        idColumn.setCellValueFactory(cellData -> cellData.getValue().custIDProperty().asObject());
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().custNameProperty());
        phoneColumn.setCellValueFactory(cellData -> cellData.getValue().custPhoneProperty());       
        custTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(reload){
            setIdNamePhone();
            }
        });
        apptTimeEndBox.setItems(apptTimeList);
        apptTimeStBox.setItems(apptTimeList);
        apptTypeBox.setItems(apptTypeList);
    }    
}
