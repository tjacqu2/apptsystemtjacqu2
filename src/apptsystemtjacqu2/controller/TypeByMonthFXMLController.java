/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import apptsystemtjacqu2.model.Appointments;
import apptsystemtjacqu2.model.Report;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class TypeByMonthFXMLController implements Initializable {
    @FXML
    private TableView<Report> apptByTypeTable;

    @FXML
    private TableColumn<Report, String> monthColumn;

    @FXML
    private TableColumn<Report, String> apptTypeColumn;

    @FXML
    private TableColumn<Report, Integer> totalColumn;
    
    @FXML
    private Label lblConsultant;
    
    private Stage secondaryStage;
    private AppointmentSystemMain mainApp;
    
    private void populateTable(ObservableList<Appointments> list) {
        boolean firstCheck = true;
        String tempMonth = "";
        int row = 0;
        int incSales = 0;
        int incFollowUp = 0;
        int incContractClose = 0;
        int incCustomerSupport = 0;
        int incTotal = 0;
        boolean monthSet = false;
        
        for(Appointments a : list) {
            
            SimpleDateFormat monthConvert = new SimpleDateFormat ("MMMM-yyyy");
            String monthFormat = monthConvert.format((java.util.Date) a.getTimeStart());
            if(firstCheck) {//set first month here to check against new month
               tempMonth = monthFormat;
               firstCheck = false;
            }
            if (tempMonth.equals(monthFormat)) {
                
                if(a.getApptType().equals("Sales")){incSales++;}
                if(a.getApptType().equals("Follow Up")){incFollowUp++;}
                if(a.getApptType().equals("Contract Close")){incContractClose++;}
                if(a.getApptType().equals("Customer Support")){incCustomerSupport++;}
            }else{
                
                //typeByMonth.setMonth(tempMonth);

                //writer.newLine();
                if(incSales > 0) {
                    Report typeByMonth = new Report();
                    typeByMonth.setMonth(tempMonth);
                    typeByMonth.setType("Sales");
                    typeByMonth.setTotal(incSales);
                    Report.getReportList().add(typeByMonth);
                    monthSet = true;
                }
                if(incFollowUp > 0){
                    Report typeByMonth = new Report();
                    if(monthSet){typeByMonth.setMonth("");}
                    else{typeByMonth.setMonth(tempMonth);}
                    monthSet = true;
                    typeByMonth.setType("Follow Up");
                    typeByMonth.setTotal(incFollowUp);
                    Report.getReportList().add(typeByMonth);
                }
                if(incContractClose > 0){
                    Report typeByMonth = new Report();
                    if(monthSet){typeByMonth.setMonth("");}
                    else{typeByMonth.setMonth(tempMonth);}
                    monthSet = true;
                    typeByMonth.setType("Contract Close");
                    typeByMonth.setTotal(incContractClose);
                    Report.getReportList().add(typeByMonth);
                }
                if(incCustomerSupport > 0){
                    Report typeByMonth = new Report();
                    if(monthSet){typeByMonth.setMonth("");}
                    else{typeByMonth.setMonth(tempMonth);}
                    monthSet = true;
                    typeByMonth.setType("Customer Support");
                    typeByMonth.setTotal(incCustomerSupport);
                    Report.getReportList().add(typeByMonth);
                }
                //row++;
                incSales = 0;
                incFollowUp = 0;
                incContractClose = 0;
                incCustomerSupport = 0;
                monthSet = false;
                if(a.getApptType().equals("Sales")){incSales++;}
                if(a.getApptType().equals("Follow Up")){incFollowUp++;}
                if(a.getApptType().equals("Contract Close")){incContractClose++;}
                if(a.getApptType().equals("Customer Support")){incCustomerSupport++;}
                tempMonth = monthFormat;
            }                    
        }        
        //row++;
        if(incSales > 0) {
            Report typeByMonth = new Report();
            typeByMonth.setMonth(tempMonth);
            typeByMonth.setType("Sales");
            typeByMonth.setTotal(incSales);
            Report.getReportList().add(typeByMonth);
            monthSet = true;
        }
        if(incFollowUp > 0){
            Report typeByMonth = new Report();
            if(monthSet){typeByMonth.setMonth("");}
            else{typeByMonth.setMonth(tempMonth);}
            monthSet = true;
            typeByMonth.setType("Follow Up");
            typeByMonth.setTotal(incFollowUp);
            Report.getReportList().add(typeByMonth);
        }
        if(incContractClose > 0){
            Report typeByMonth = new Report();
            if(monthSet){typeByMonth.setMonth("");}
            else{typeByMonth.setMonth(tempMonth);}
            monthSet = true;
            typeByMonth.setType("Contract Close");
            typeByMonth.setTotal(incContractClose);
            Report.getReportList().add(typeByMonth);
        }
        if(incCustomerSupport > 0){
            Report typeByMonth = new Report();
            if(monthSet){typeByMonth.setMonth("");}
            else{typeByMonth.setMonth(tempMonth);}
            monthSet = true;
            typeByMonth.setType("Customer Support");
            typeByMonth.setTotal(incCustomerSupport);
            Report.getReportList().add(typeByMonth);
        }
        monthSet = false;
    }
    
    @FXML
    private void handleBtnCancel() {
        Report.getReportList().clear();
        secondaryStage.close();
    }
    
    public void setMainApp(AppointmentSystemMain mainApp) {
        this.mainApp = mainApp;
        apptByTypeTable.setItems(Report.getReportList());
    }
    
    
    public void setSecondaryStage(Stage secondaryStage) {
		this.secondaryStage = secondaryStage;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        populateTable(GenerateReportsFXMLController.apptSchedule);
        lblConsultant.setText("Consultant: " + LogInFXMLController.popUserName);
        monthColumn.setCellValueFactory(cellData -> cellData.getValue().monthProperty());
        apptTypeColumn.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
        totalColumn.setCellValueFactory(cellData -> cellData.getValue().totalProperty().asObject());
    }    
}
