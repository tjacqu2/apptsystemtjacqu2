/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import apptsystemtjacqu2.model.Appointments;
import apptsystemtjacqu2.model.Report;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class ConsultantTotalApptsByMonthFXMLController implements Initializable {
    @FXML
    private TableView<Report> ApptTotalTable;

    @FXML
    private TableColumn<Report, String> monthColumn;

    @FXML
    private TableColumn<Report, String> consultantColumn;

    @FXML
    private TableColumn<Report, Integer> apptTotalColumn;
    @FXML
    private Label lblConsultant;
    private Stage secondaryStage;
    private AppointmentSystemMain mainApp;
    
    private void populateTable(ObservableList<Appointments> list) {
    boolean firstCheck = true;
    String tempMonth = "";
    int incTotal = 0;
    for(Appointments a : list) {
            
            SimpleDateFormat monthConvert = new SimpleDateFormat ("MMMM-yyyy");
            String monthFormat = monthConvert.format((java.util.Date) a.getTimeStart());
            if(firstCheck) {//set first month here to check against new month
               tempMonth = monthFormat;
               firstCheck = false;
            }
            if (tempMonth.equals(monthFormat)) {
                
                if(a.getApptType().equals("Sales")){incTotal++;}
                if(a.getApptType().equals("Follow Up")){incTotal++;}
                if(a.getApptType().equals("Contract Close")){incTotal++;}
                if(a.getApptType().equals("Customer Support")){incTotal++;}
            }else{
                
                //typeByMonth.setMonth(tempMonth);

                //writer.newLine();
                if(incTotal > 0) {
                    Report typeByMonth = new Report();
                    typeByMonth.setMonth(tempMonth);
                    typeByMonth.setTotal(incTotal);
                    Report.getReportList().add(typeByMonth);
                }
                //row++;
                incTotal = 0;
                //consultSet = false;
                if(a.getApptType().equals("Sales")){incTotal++;}
                if(a.getApptType().equals("Follow Up")){incTotal++;}
                if(a.getApptType().equals("Contract Close")){incTotal++;}
                if(a.getApptType().equals("Customer Support")){incTotal++;}
                tempMonth = monthFormat;
            }                    
        }        
        //row++;
        if(incTotal > 0) {
            Report typeByMonth = new Report();
            typeByMonth.setMonth(tempMonth);
            typeByMonth.setTotal(incTotal);
            Report.getReportList().add(typeByMonth);
            //consultSet = true;
        }
    }
    
    @FXML
    private void handleBtnCancel() {
        Report.getReportList().clear();
        secondaryStage.close();
    }
    
    public void setMainApp(AppointmentSystemMain mainApp) {
        this.mainApp = mainApp;
        ApptTotalTable.setItems(Report.getReportList());
    }
    
    
    public void setSecondaryStage(Stage secondaryStage) {
		this.secondaryStage = secondaryStage;
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        populateTable(GenerateReportsFXMLController.apptSchedule);
        lblConsultant.setText("Consultant: " + LogInFXMLController.popUserName);
        monthColumn.setCellValueFactory(cellData -> cellData.getValue().monthProperty());
        apptTotalColumn.setCellValueFactory(cellData -> cellData.getValue().totalProperty().asObject());
    }    
    
}
