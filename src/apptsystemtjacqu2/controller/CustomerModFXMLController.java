/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import static apptsystemtjacqu2.controller.LogInFXMLController.popUserName;
import apptsystemtjacqu2.model.Customer;
import static apptsystemtjacqu2.model.Customer.getCustomerList;
import apptsystemtjacqu2.model.DBAccess;
import java.net.URL;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class CustomerModFXMLController implements Initializable {
    
    @FXML
    private TextField custIDTextField;
    
    @FXML
    private TextField custNameTextField;

    @FXML
    private TextField custAddressTextField;

    @FXML
    private TextField custAddress2TextField;

    @FXML
    private TextField custCityTextField;

    @FXML
    private TextField custZipTextField;

    @FXML
    private TextField custCountryTextField;

    @FXML
    private TextField custPhoneTextField;

    @FXML
    private TableView<Customer> custModTable;

    @FXML
    private TableColumn<Customer, Integer> idColumn;

    @FXML
    private TableColumn<Customer, String> nameColumn;

    @FXML
    private TableColumn<Customer, String> phoneColumn;

    @FXML
    private TextField custSearchTextField;
    
    private AppointmentSystemMain mainApp;
    private Stage secondaryStage;
    public static boolean  reload;
    /*private void handleCustModSearch() {
        String searchText = custSearchTextField.getText();
        FilteredList<Customer> searchCustResults = searchCustomers(searchText);
        SortedList<Customer> sortedData = new SortedList<>(searchCustResults);
        sortedData.comparatorProperty().bind(custModTable.comparatorProperty());
        custModTable.setItems(sortedData);
        if (searchCustResults.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Customer Not Found");
            alert.setContentText("No customer was found. Re-try search");
            alert.showAndWait();
        }
    }*/
    
    /**
     * searches part by part ID or name
     * @param s search string
     * @return part match
     */
    /*private FilteredList<Customer> searchCustomers(String cust){
        
           return Customer.getCustomerList().filtered(c -> c.getCustName().toLowerCase().contains(cust.toLowerCase())); 
        
    }*/
    
    private void setCustomer() throws SQLException{
        Customer loadTable  = custModTable.getSelectionModel().getSelectedItem();
        //Customer loadTable  = custModTable.getSelectionModel().
        loadTable = DBAccess.getSingleCustomerFromDB(loadTable.getCustID());
        custIDTextField.setText(Integer.toString(loadTable.getCustID()));
        custNameTextField.setText(loadTable.getCustName());
        custAddressTextField.setText(loadTable.getCustAddress());
        custAddress2TextField.setText(loadTable.getCustAddress2());
        custCityTextField.setText(loadTable.getCustCity());
        custZipTextField.setText(loadTable.getCustZip());
        custCountryTextField.setText(loadTable.getCustCountry());
        custPhoneTextField.setText(loadTable.getCustPhone());
    }
    
    @FXML
    private void handleBtnCustModSave() throws SQLException {
        if(custIDTextField.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Customer not specified");
            alert.setContentText("Please choose customer from list");
            alert.showAndWait();
        }
        else if(custNameTextField.getText().isEmpty() || custAddressTextField.getText().isEmpty()
                || custAddress2TextField.getText().isEmpty() || custCityTextField.getText().isEmpty() || custZipTextField.getText().isEmpty()
                || custCountryTextField.getText().isEmpty() || custPhoneTextField.getText().isEmpty()){
                throw new NullPointerException();//One or more fields not populated
        }else{
            DBAccess.modifyCustomerRecord(Integer.parseInt(custIDTextField.getText()), custNameTextField.getText(), custAddressTextField.getText(),
                custAddress2TextField.getText(), custCityTextField.getText(), custZipTextField.getText(),
                custCountryTextField.getText(), custPhoneTextField.getText(), popUserName, 1);
            reload = false;
            DBAccess.getDBCustomerList();
            Collections.sort(getCustomerList(),(m1, m2) -> m1.getCustName().compareTo(m2.getCustName()));
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Confirmed");
            alert.setHeaderText("Customer modification comfirmed");
            alert.setContentText("Customer Modified");
            alert.showAndWait();
            secondaryStage.close();
        }        
    }
    
    @FXML
    private void handleBtnCustModCancel() {
        reload = true;
        secondaryStage.close();
    }
    
    @FXML
    private void handleCustomerDelete() throws SQLException{
        if(custIDTextField.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Customer not specified");
            alert.setContentText("Please choose customer from list");
            alert.showAndWait();
        }else{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Alert");
            alert.setHeaderText("Customer and associated appointments will be deleted!");
            alert.setContentText("Are you sure?");
            ButtonType button1 = new ButtonType("Yes", ButtonBar.ButtonData.YES);
            ButtonType button2 = new ButtonType("No", ButtonBar.ButtonData.NO);
            alert.getButtonTypes().setAll(button1, button2);
            Optional<ButtonType> result = alert.showAndWait();

            result.filter(response -> result.get() == button1)
            .ifPresent(response -> {int Id = Integer.parseInt(custIDTextField.getText());
            
                try {
                    DBAccess.deleteCustomerRecord(Id);
                    reload = false;
                    DBAccess.getDBCustomerList();
                    Collections.sort(getCustomerList(),(m1, m2) -> m1.getCustName().compareTo(m2.getCustName()));
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerModFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            
        }
    }
    
    
    public void setMainApp(AppointmentSystemMain mainApp) {
            this.mainApp = mainApp;
            Collections.sort(getCustomerList(),(m1, m2) -> m1.getCustName().compareTo(m2.getCustName()));
            custModTable.setItems(Customer.getCustomerList());
    }
    
    public void setSecondaryStage(Stage secondaryStage) {
		this.secondaryStage = secondaryStage;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        reload = true;
        idColumn.setCellValueFactory(cellData -> cellData.getValue().custIDProperty().asObject());
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().custNameProperty());
        phoneColumn.setCellValueFactory(cellData -> cellData.getValue().custPhoneProperty());
        custModTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {try {
            if(reload){
                setCustomer();
            }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        });
    }    
    
}
