/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import static apptsystemtjacqu2.controller.LogInFXMLController.popUserName;
import static apptsystemtjacqu2.model.Customer.getCustomerList;
import apptsystemtjacqu2.model.DBAccess;
import java.net.URL;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class CustomerAddFXMLController implements Initializable {
    
    @FXML
    private TextField custIDTextField;

    @FXML
    private TextField custNameTextField;

    @FXML
    private TextField custAddressTextField;
    
    @FXML
    private TextField custAddress2TextField;
    
    @FXML
    private TextField custCityTextField;

    @FXML
    private TextField custCountryTextField;

    @FXML
    private TextField custZipTextField;

    @FXML
    private TextField custPhoneTextField;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;

    private AppointmentSystemMain mainApp;
    private Stage secondaryStage;
    @FXML
    private void handleBtnCustSave() throws SQLException {
        
        try{
            if(custNameTextField.getText().isEmpty() || custAddressTextField.getText().isEmpty() || custAddress2TextField.getText().isEmpty()
            || custZipTextField.getText().isEmpty() || custPhoneTextField.getText().isEmpty() ||custCityTextField.getText().isEmpty() || custCountryTextField.getText().isEmpty()){
                throw new NullPointerException();
            }else{
            
            DBAccess.addCustomerRecord(custNameTextField.getText(), 1, custAddressTextField.getText(), custAddress2TextField.getText(),
            custZipTextField.getText(), custPhoneTextField.getText(), custCityTextField.getText(), custCountryTextField.getText(), 
            popUserName, popUserName);
            DBAccess.getDBCustomerList();
            Collections.sort(getCustomerList(),(m1, m2) -> m1.getCustName().compareTo(m2.getCustName()));
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Alert");
            alert.setHeaderText("Customer Added");
            alert.setContentText("Would you like to add another customer?");
            ButtonType button1 = new ButtonType("Yes");
            ButtonType button2 = new ButtonType("No", ButtonBar.ButtonData.NO);
            alert.getButtonTypes().setAll(button1, button2);
            Optional<ButtonType> result = alert.showAndWait();

            result.filter(response -> result.get() == button2)
            .ifPresent(response -> secondaryStage.close());
            clearFields();
            }
        }catch(NullPointerException npe){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Exception");
            alert.setHeaderText("All Fields Are Not Populated");
            alert.setContentText("Please populate all fields");
            alert.showAndWait();
        }
    }
    
    private void clearFields() {
       custNameTextField.clear();
       custAddressTextField.clear();
       custAddress2TextField.clear();
       custZipTextField.clear();
       custPhoneTextField.clear();
       custCityTextField.clear();
       custCountryTextField.clear();
    }
    
    @FXML
    private void handleBtnCustCancel() {
       secondaryStage.close(); 
    }
    
    public void setMainApp(AppointmentSystemMain mainApp) {
        this.mainApp = mainApp;
    }
    
    public void setSecondaryStage(Stage secondaryStage) {
		this.secondaryStage = secondaryStage;
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
