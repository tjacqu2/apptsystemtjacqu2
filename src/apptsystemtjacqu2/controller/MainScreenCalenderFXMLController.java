/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import apptsystemtjacqu2.model.Appointments;
import static apptsystemtjacqu2.model.Appointments.getApptListMonthly;
import static apptsystemtjacqu2.model.Appointments.getApptListWeekly;
import apptsystemtjacqu2.model.DBAccess;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Collections;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class MainScreenCalenderFXMLController implements Initializable {
    
    @FXML
    private Label lblWeek;
    
    @FXML
    private Label lblMonth;
    
    @FXML
    private Label lblDate;
    
    @FXML
    private RadioButton btnRadioWeekly;

    @FXML
    private RadioButton btnRadioMonthly;

    @FXML
    private RadioButton btnRadioListView;

    @FXML
    private RadioButton btnRadioCalView;

    @FXML
    private Button btnMakeAppt;

    @FXML
    private Button btnAddCustomer;

    @FXML
    private Button btnModCustomer;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnExit;

    @FXML
    private Button btnPrevious;

    @FXML
    private ColorPicker menuColorPicker;

    @FXML
    private Button btnNext;

    @FXML
    private TableView<Appointments> apptTable;

    @FXML
    private TableColumn<Appointments, String> dateColumn;

    @FXML
    private TableColumn<Appointments, String> apptStartColumn;

    @FXML
    private TableColumn<Appointments, String> apptEndColumn;

    @FXML
    private TableColumn<Appointments, String> apptTypeColumn;

    @FXML
    private TableColumn<Appointments, String> customerColumn;

    @FXML
    private TableColumn<Appointments, String> phoneColumn;
    
    @FXML
    private TableColumn<Appointments, String> contactColumn;

    @FXML
    private TableColumn<Appointments, String> titleColumn;
    
    @FXML
    private Label lblConsultant;
    
    private Pane pane11;
    private Stage secondaryStage;
    private AppointmentSystemMain mainApp;
    //private static ObservableList<Appointments> apptSchedule;
    //private static ObservableList<Appointments> apptScheduleWeekly;
    //private static ObservableList<Appointments> apptScheduleMonthly;
    
    public static int newWeek = 0;
    public static int newMonth = 1;
    public static String newYear = "";
    public static boolean month = false;
    public static int popApptId;
    public static String popDateMS;
    public static String popApptSt;
    public static String popApptEnd;
    public static String popApptType;
    public static int popCustId;
    public static String popCustomer;
    public static String popPhone;
    public static String popContact;
    public static String popTitle;
    

    public MainScreenCalenderFXMLController() {
        
    }
    
    @FXML
    private void handleBtnMakeAppt() throws Exception{
        mainApp.showMakeAppointment();
    }
    
    @FXML
    private void handleBtnModAppt() {
        Appointments appt = getApptTable().getSelectionModel().getSelectedItem();
        if(appt !=  null){
            popApptId = appt.getApptId();
            popDateMS = appt.getDate();
            popApptSt = appt.getTimeStartSt();
            String tempSt1 = popApptSt.substring(0, 5);
            String tempSt2 = popApptSt.substring(9, 11);
            popApptSt = tempSt1 + " " + tempSt2;
            popApptEnd = appt.getTimeEndSt();
            String tempEnd1 = popApptEnd.substring(0, 5);
            String tempEnd2 = popApptEnd.substring(9, 11);
            popApptEnd = tempEnd1 + " " + tempEnd2;
            popApptType = appt.getApptType();
            popCustomer = appt.getCustomer();
            popCustId = appt.getCustomerId();
            popPhone = appt.getPhone();
            popContact = appt.getContact();
            popTitle = appt.getTitle();
            mainApp.showModifyAppt();
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("No Appointment Selection Made");
            alert.setContentText("Please select appointment from table");
            alert.showAndWait();
        }
    }
    
    @FXML
    private void handleBtnAddCustomer() {
        mainApp.showAddCustomer();
    }
    
    @FXML
    private void handleBtnModCustomer() {     
        mainApp.showModCustomer();
    }
    
    @FXML
    private void handleBtnGenerateReport() {
        mainApp.showGenerateReport();
    }
    
    @FXML
    private void handleBtnForward() throws SQLException {
        if(!month) {
            ++newWeek;
            if(newWeek == 55){
                newWeek = 1;
            //if(newWeek == 54){
                //newWeek = 0;
                int newYearInt = Integer.parseInt(newYear);
                ++newYearInt;
                newYear = Integer.toString(newYearInt);
            }
            DBAccess.getConsultantScheduleDataWeekly(LogInFXMLController.popUserName, newWeek);
            Collections.sort(getApptListWeekly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
            getApptTable().setItems(getApptListWeekly());
            lblMonth.setText("");
            int weekAdjust = newWeek;
            lblWeek.setText("Week Number " + weekAdjust + " of " + newYear);
        }else{
            ++newMonth;
            if(newMonth == 13){
                newMonth = 1;
                int newYearInt = Integer.parseInt(newYear);
                ++newYearInt;
                newYear = Integer.toString(newYearInt);
            }
            DBAccess.getConsultantScheduleDataMonthly(LogInFXMLController.popUserName, newMonth, newYear);
            Collections.sort(getApptListMonthly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
            getApptTable().setItems(getApptListMonthly());
            lblWeek.setText("");
            lblMonth.setText(newMonth + "-" + newYear);
        }
    }
    
    @FXML
    private void handleBtnBack() throws SQLException {
        //if weekly go back newWeek
        //if monthly go back newMonth
        if(!month) {
            --newWeek;
            if(newWeek == 0){
                newWeek = 54;
            //if(newWeek == -1){
                //newWeek = 53;
                int newYearInt = Integer.parseInt(newYear);
                --newYearInt;
                newYear = Integer.toString(newYearInt);
            }
            DBAccess.getConsultantScheduleDataWeekly(LogInFXMLController.popUserName, newWeek);
            Collections.sort(getApptListWeekly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
            getApptTable().setItems(getApptListWeekly());
            lblMonth.setText("");
            int weekAdjust = newWeek;
            lblWeek.setText("Week Number " + weekAdjust + " of " + newYear);
        }else{
            --newMonth;
            if (newMonth == 0){
                newMonth = 12;
                int newYearInt = Integer.parseInt(newYear);
                --newYearInt;
                newYear = Integer.toString(newYearInt);
            }
            DBAccess.getConsultantScheduleDataMonthly(LogInFXMLController.popUserName, newMonth, newYear);
            Collections.sort(getApptListMonthly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
            getApptTable().setItems(getApptListMonthly());
            lblWeek.setText("");
            lblMonth.setText(newMonth + "-" + newYear);
        }
    }
    
    @FXML
    private void handleBtnWeekly() {
        //if switched from monthly, go to 1st week of shown month
        month = false;
        newWeek = getWeek();
        newYear = getYear();
        lblMonth.setText("");
        int weekAdjust = newWeek + 1;
        lblWeek.setText("Week Number " + weekAdjust + " of " + newYear);
        try {
            setMainApp(mainApp);
        } catch (SQLException ex) {
            Logger.getLogger(MainScreenCalenderFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void handleBtnMonthly() {
        //if switched from weekly, go to month where beginning of week shown
        month = true;
        newYear = getYear();
        newMonth = getMonth();
        lblWeek.setText("");
        lblMonth.setText(newMonth + "-" + newYear);
        try {
            setMainApp(mainApp);
        } catch (SQLException ex) {
            Logger.getLogger(MainScreenCalenderFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void handleBtnApptList() {
        
    }
    
    @FXML
    private void handleBtnApptCalender() {
        
    }
    
    @FXML
    private void handleApptModify() {
        
    }
    
    @FXML
    private void handleMenuColorPicker() {
        
    }
    
    @FXML
    private void handleSystemExit() throws SQLException{
        DBAccess.dbConnectionClose(DBAccess.ps, DBAccess.dbConnection);
        System.exit(0);
    }
    
    private static int getWeek() {
        int weekNum = 0;
        LocalDate ld = LocalDate.now();
        WeekFields wf = WeekFields.of(Locale.US);
        weekNum = ld.get(wf.weekOfWeekBasedYear());
        return weekNum;
    }
    
    private static int getMonth() {
        int monthNum = 1;
        monthNum = Integer.parseInt(LocalDate.now().toString().substring(5, 7));
        return monthNum;
    }
    
    private static String getYear() {
        String year = "";
        year = LocalDate.now().toString().substring(0,4);
        return year;
    }
    private ObservableList<Appointments> formatListAsWeekly(ObservableList<Appointments> list, String Consultant, String weekStart) {
        //format dates
        return list;
    }
    
    private ObservableList<Appointments> formatListAsMonthly(ObservableList<Appointments> list, String Consultant, String monthStart) {
        //format Dates
        return list;
    }
    
    public void setMainApp(AppointmentSystemMain mainApp) throws SQLException {
        this.mainApp = mainApp;
        if(!month){//week
            DBAccess.getConsultantScheduleDataWeekly(LogInFXMLController.popUserName, newWeek);
            Collections.sort(getApptListWeekly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
            getApptTable().setItems(getApptListWeekly());
            
        }else{//month
            DBAccess.getConsultantScheduleDataMonthly(LogInFXMLController.popUserName, newMonth, newYear);
            Collections.sort(getApptListMonthly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
            getApptTable().setItems(getApptListMonthly());
        }
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb){
        newWeek = getWeek();
        newYear = getYear();
        lblMonth.setText("");
        //int weekAdjust = newWeek + 1;
        lblWeek.setText("Week Number " + newWeek + " of " + newYear);
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM-dd-yyyy");
        String formattedDate = date.format(formatter);
        lblDate.setText(formattedDate);
        lblConsultant.setText("Consultant: " + LogInFXMLController.popUserName);
        getDateColumn().setCellValueFactory(cellData -> cellData.getValue().dateProperty());
        getApptStartColumn().setCellValueFactory(cellData -> cellData.getValue().timeStartPropertySt());
        getApptEndColumn().setCellValueFactory(cellData -> cellData.getValue().timeEndStProperty());
        getApptTypeColumn().setCellValueFactory(cellData -> cellData.getValue().apptTypeProperty());
        getCustomerColumn().setCellValueFactory(cellData -> cellData.getValue().customerProperty());
        getPhoneColumn().setCellValueFactory(cellData -> cellData.getValue().phoneProperty());
        getContactColumn().setCellValueFactory(cellData -> cellData.getValue().contactProperty());
        getTitleColumn().setCellValueFactory(cellData -> cellData.getValue().titleProperty());
    }    

    /**
     * @return the dateColumn
     */
    public TableColumn<Appointments, String> getDateColumn() {
        return dateColumn;
    }

    /**
     * @return the apptStartColumn
     */
    public TableColumn<Appointments, String> getApptStartColumn() {
        return apptStartColumn;
    }

    /**
     * @return the apptEndColumn
     */
    public TableColumn<Appointments, String> getApptEndColumn() {
        return apptEndColumn;
    }

    /**
     * @return the apptTypeColumn
     */
    public TableColumn<Appointments, String> getApptTypeColumn() {
        return apptTypeColumn;
    }

    /**
     * @return the customerColumn
     */
    public TableColumn<Appointments, String> getCustomerColumn() {
        return customerColumn;
    }

    /**
     * @return the phoneColumn
     */
    public TableColumn<Appointments, String> getPhoneColumn() {
        return phoneColumn;
    }

    /**
     * @return the contactColumn
     */
    public TableColumn<Appointments, String> getContactColumn() {
        return contactColumn;
    }

    /**
     * @return the titleColumn
     */
    public TableColumn<Appointments, String> getTitleColumn() {
        return titleColumn;
    }

    /**
     * @return the apptTable
     */
    public TableView<Appointments> getApptTable() {
        return apptTable;
    }
    
    
}
