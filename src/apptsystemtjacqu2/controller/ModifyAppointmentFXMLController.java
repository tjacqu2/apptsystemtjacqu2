/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.controller;

import apptsystemtjacqu2.AppointmentSystemMain;
import static apptsystemtjacqu2.controller.MainScreenCalenderFXMLController.month;
import static apptsystemtjacqu2.controller.MainScreenCalenderFXMLController.popCustId;
import static apptsystemtjacqu2.controller.MainScreenCalenderFXMLController.popCustomer;
import static apptsystemtjacqu2.controller.MainScreenCalenderFXMLController.popPhone;
import apptsystemtjacqu2.model.Appointments;
import static apptsystemtjacqu2.model.Appointments.getApptList;
import static apptsystemtjacqu2.model.Appointments.getApptListMonthly;
import static apptsystemtjacqu2.model.Appointments.getApptListWeekly;
import apptsystemtjacqu2.model.DBAccess;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class ModifyAppointmentFXMLController implements Initializable {
    
    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;

    @FXML
    private DatePicker datePickerField;

    @FXML
    private ChoiceBox<String> timeStartDropDown;

    @FXML
    private ChoiceBox<String> timeEndDropDown;

    @FXML
    private ChoiceBox<String> apptTypeDropDown;

    @FXML
    private TextField contactTextfield;

    @FXML
    private TextField titleTextfield;
    
    @FXML
    private Label lblCustId;
    
    @FXML
    private Label lblCustomer;

    @FXML
    private Label lblPhone;
    
    private ObservableList<String> apptTimeList = FXCollections.observableArrayList(
            "08:00 AM", "08:15 AM", "08:30 AM", "08:45 AM", "09:00 AM", "09:15 AM", "09:30 AM", "09:45 AM", "10:00 AM", "10:15 AM", "10:30 AM", 
            "10:45 AM", "11:00 AM", "11:15 AM", "11:30 AM", "11:45 AM", "12:00 PM", "12:15 PM", "12:30 PM", "12:45 PM", "01:00 PM", "01:15 PM",
            "01:30 PM", "01:45 PM", "02:00 PM", "02:15 PM", "02:30 PM", "02:45 PM", "03:00 PM", "03:15 PM", "03:30 PM", "03:45 PM", "04:00 PM", 
            "04:15 PM", "04:30 PM", "04:45 PM", "05:00 PM");
    private ObservableList<String> apptTypeList = FXCollections.observableArrayList("Sales", "Follow Up", "Contract Close", "Customer Support");
    
    private AppointmentSystemMain mainApp;
    private Stage secondaryStage;
    private static ObservableList<Appointments> apptScheduleAll;
    private static LocalDate dateOriginal;
    @FXML
    private void handleBtnSave() {
        boolean save = true;
        if(!contactTextfield.getText().isEmpty() && !titleTextfield.getText().isEmpty() && datePickerField.getValue() != null 
                && timeStartDropDown.getValue() != null && timeEndDropDown.getValue() != null && apptTypeDropDown.getValue() != null) {
            try{
                LocalTime timeChkStart = DBAccess.timeStringToLocalTimeMilitary(timeStartDropDown.getValue());
                LocalTime timeChkEnd = DBAccess.timeStringToLocalTimeMilitary(timeEndDropDown.getValue());
                if(timeChkEnd.minusMinutes(1).isBefore(timeChkStart)){
                    throw new Exception();
                }
                try{
                    if(checkConflictingAppointments()) {
                        throw new Exception();
                    }
                    try{
                        String date = datePickerField.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                        DBAccess.updateAppointment(MainScreenCalenderFXMLController.popApptId, 
                                contactTextfield.getText(), titleTextfield.getText(), date, timeStartDropDown.getValue(), 
                                timeEndDropDown.getValue(), apptTypeDropDown.getValue(), LogInFXMLController.popUserName);//apptTimeStBox.getValue(),apptTimeEndBox.getValue()
                        if(!month){
                        DBAccess.getConsultantScheduleDataWeekly(LogInFXMLController.popUserName, MainScreenCalenderFXMLController.newWeek);
                        Collections.sort(getApptListWeekly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
                        }
                        else{
                        DBAccess.getConsultantScheduleDataMonthly(LogInFXMLController.popUserName, MainScreenCalenderFXMLController.newMonth, MainScreenCalenderFXMLController.newYear);
                        Collections.sort(getApptListMonthly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
                        }
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Confirmed");
                        alert.setHeaderText("Appointment modification comfirmed");
                        alert.setContentText("Appointment Modified");
                        alert.showAndWait();
                        secondaryStage.close();
                    }catch(SQLException e){
                        System.out.println("Appointment Update " + e.getMessage());
                    }
                }catch(Exception e){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Alert");
                    alert.setHeaderText("Conflicting Appointment in This Time Slot");
                    alert.setContentText("Please change appointment time");
                    alert.showAndWait();
                }
            }catch(Exception e) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Alert");
                alert.setHeaderText("Start time cannot be after or equal to end time");
                alert.setContentText("Please change appointment times");
                alert.showAndWait(); 
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("All Fields Are Not Populated");
            alert.setContentText("Please populate all fields");
            alert.showAndWait();
        }
    }
    
    @FXML
    private void handleBtnCancel() {
        secondaryStage.close();
    }
    
    @FXML
    private void handleBtnDeleteAppt() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Appointment will be deleted!");
        alert.setContentText("Are you sure?");
        ButtonType button1 = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType button2 = new ButtonType("No", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(button1, button2);
        Optional<ButtonType> result = alert.showAndWait();

        result.filter(response -> result.get() == button1)
        .ifPresent(response -> {
            try {
                DBAccess.deleteAppointment(MainScreenCalenderFXMLController.popApptId);
                if(!month){
                    DBAccess.getConsultantScheduleDataWeekly(LogInFXMLController.popUserName, MainScreenCalenderFXMLController.newWeek);
                    Collections.sort(getApptListWeekly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
                }
                else{
                    DBAccess.getConsultantScheduleDataMonthly(LogInFXMLController.popUserName, MainScreenCalenderFXMLController.newMonth, MainScreenCalenderFXMLController.newYear);
                    Collections.sort(getApptListMonthly(),(m1, m2) -> m1.getTimeStart().compareTo(m2.getTimeStart()));
                }
                
                secondaryStage.close();
            } catch (SQLException ex) {
                Logger.getLogger(CustomerModFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
    }
    
    private boolean checkConflictingAppointments() throws SQLException, ParseException {
        boolean conflict = false;
        boolean noConflictFlag = false;
        boolean frontEndFlag = false;
        boolean backEndFlag = false;
        DBAccess.getConsultantScheduleDataAll();
        ObservableList<Appointments> list = getApptList();
        for(Appointments a : list) {
            String stConcat = timeStartDropDown.getValue().substring(0, 5);
            String endConcat = timeEndDropDown.getValue().substring(0, 5);
            LocalTime apptSt = LocalTime.parse(stConcat);//.plusMinutes(1);//string to LocalTime
            LocalTime apptEnd = LocalTime.parse(endConcat);//.minusMinutes(1);//string to LocalTime
            LocalTime dbStartTi = LocalTime.parse(a.getTimeStartSt().substring(0, 5));//string to LocalTime
            LocalTime dbEndTi = LocalTime.parse(a.getTimeEndSt().substring(0, 5));//string to LocalTime            
            /*reformat date and convert to string*/
            String outDateStr = datePickerField.getValue().format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
            //are there any appointments on that date?
            if (a.getDate().equals(outDateStr)) {
            //Does this consultant have any appointments with any customer in the prospective timeframe?
                //current logged in consultant
                if (a.getConsultant().equals(LogInFXMLController.popUserName)) {
                    if(dateOriginal == datePickerField.getValue()){
                        //if modified appointment times stayed the same, there is no conflict
                        if((apptSt == dbStartTi && apptEnd == dbEndTi) 
                                || (apptSt.plusMinutes(1).isAfter(dbStartTi) && apptEnd.minusMinutes(1).isBefore(dbEndTi))){
                            noConflictFlag = true;
                        }
                        //if either modified start or end time are within original appt time, check time that is not within original time for conflict 
                        if(!noConflictFlag){
                            if((backEndFlag) && (apptEnd.plusMinutes(1).isAfter(dbStartTi) 
                                    && apptEnd.minusMinutes(1).isBefore(dbEndTi))) {//check for apptEnd conflicts
                                //apptEnd conflicts
                                conflict = true;
                            }
                            if(!backEndFlag) {
                                if((apptSt == dbStartTi || apptSt == dbEndTi) 
                                    || (apptSt.plusMinutes(1).isAfter(dbStartTi) && apptSt.minusMinutes(1).isBefore(dbEndTi))) {
                                    //no conflict yet
                                    backEndFlag = true;//set backEndFlag for next appointment on this date if any
                                }
                            }
                            if((frontEndFlag) && (apptSt.plusMinutes(1).isAfter(dbStartTi) 
                                    && apptSt.minusMinutes(1).isBefore(dbEndTi))) {//check for apptStart conflicts
                                //apptStart conflicts
                                conflict = true;
                            }
                            if(!frontEndFlag) {
                                if((apptEnd == dbStartTi || apptEnd == dbEndTi) 
                                    || (apptEnd.plusMinutes(1).isAfter(dbStartTi) && apptEnd.minusMinutes(1).isBefore(dbEndTi))) {
                                    //no conflict yet
                                    frontEndFlag = true;//set frontEndFlag for next appointment on this date if any
                                }
                            }
                        }
                    }    
                    //if neither modified start or end time are within original time, check both times for conflict
                    if(!frontEndFlag && !backEndFlag && !noConflictFlag){
                        if((apptSt.plusMinutes(1).isAfter(dbStartTi) && apptSt.isBefore(dbEndTi))
                                || (apptEnd.isAfter(dbStartTi) && apptEnd.minusMinutes(1).isBefore(dbEndTi))){
                        conflict = true;
                        System.out.println("Current consultant has appointment");
                        }
                    }
                }else if(a.getCustomerId() == Integer.parseInt(lblCustId.getText().substring(13))){
                //Does this specific customer have any appointments with any consultant for this appointment timeframe?    
                    if((apptSt.plusMinutes(1).isAfter(dbStartTi) && apptSt.isBefore(dbEndTi))
                            || (apptEnd.isAfter(dbStartTi) && apptEnd.minusMinutes(1).isBefore(dbEndTi))){
                    conflict = true;
                    System.out.println("Another Consultant has appointment with same customer");
                    }
                }                
            }           
        }
        return conflict;
    }
    
    public void setSecondaryStage(Stage secondaryStage) {
	this.secondaryStage = secondaryStage;
    }
    
    public void setMainApp(AppointmentSystemMain mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(MainScreenCalenderFXMLController.popDateMS, formatter);
        dateOriginal = date;
        datePickerField.setValue(date);
        timeStartDropDown.setItems(apptTimeList);
        timeEndDropDown.setItems(apptTimeList);
        apptTypeDropDown.setItems(apptTypeList);
        timeStartDropDown.setValue(MainScreenCalenderFXMLController.popApptSt);
        timeEndDropDown.setValue(MainScreenCalenderFXMLController.popApptEnd);
        apptTypeDropDown.setValue(MainScreenCalenderFXMLController.popApptType);
        contactTextfield.setText(MainScreenCalenderFXMLController.popContact);
        titleTextfield.setText(MainScreenCalenderFXMLController.popTitle);
        lblCustId.setText("Customer ID: " + Integer.toString(popCustId));
        lblCustomer.setText("Customer: " + popCustomer);
        lblPhone.setText("Phone: " + popPhone);
    }    
    
}
