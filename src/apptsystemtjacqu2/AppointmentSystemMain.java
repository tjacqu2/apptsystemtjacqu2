/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2;

import apptsystemtjacqu2.controller.AppointmentFXMLController;
import apptsystemtjacqu2.controller.CustomerAddFXMLController;
import apptsystemtjacqu2.controller.CustomerModFXMLController;
import apptsystemtjacqu2.controller.GenerateReportsFXMLController;
import apptsystemtjacqu2.controller.LogInFXMLController;
import apptsystemtjacqu2.controller.MainScreenCalenderFXMLController;
import apptsystemtjacqu2.controller.ConsultantReportFXMLController;
import apptsystemtjacqu2.controller.ConsultantTotalApptsByMonthFXMLController;
import apptsystemtjacqu2.controller.ModifyAppointmentFXMLController;
import apptsystemtjacqu2.controller.TypeByMonthFXMLController;
import apptsystemtjacqu2.model.DBAccess;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
//uses
/**
 *
 * @author tomjacques
 */
public class AppointmentSystemMain extends Application {
    
    private Stage apptStage;
    private Stage logInStage;
    private BorderPane mainPane;
    
    @Override
    public void start(Stage primaryStage) throws Exception {    
        this.apptStage = primaryStage;
        //this.apptStage.setTitle("Appointment Calender");
        mainPane = new BorderPane();
        DBAccess.getDBCustomerList();
        showLogIn();
        
    }
    
    
    
    private void initRootLayout() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/RootLayoutFXML.fxml"));
            mainPane = (BorderPane) loader.load();
            Scene scene = new Scene(mainPane,955,600);
            getApptStage().setScene(scene);
            getApptStage().show();
        }catch(Exception e) {
           e.printStackTrace();
        }
    }
    
    
    
    private void showLogIn() {
        //setup stage and scene for login screen
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/LogInFXML.fxml"));    
            BorderPane logIn = (BorderPane) loader.load();
            Scene scene = new Scene(logIn);
            getApptStage().setScene(scene);
            getApptStage().setTitle(LogInFXMLController.logInTitle);
            getApptStage().show();
            
            LogInFXMLController controller = loader.getController();
            controller.setMainApp(this);
        }catch(Exception e){
           e.printStackTrace(); 
        }
    }
    
    public void showMainCalenderLayout() {
        //setup stage and scene for main calender layout
        try{
            initRootLayout();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/MainScreenCalenderFXML.fxml"));
            BorderPane calender = (BorderPane) loader.load();
            getMainPane().setCenter(calender);
            getApptStage().setTitle("Appointment Calender");
            MainScreenCalenderFXMLController controller = loader.getController();
            try {
                controller.setMainApp(this);
            } catch (SQLException ex) {
                Logger.getLogger(AppointmentSystemMain.class.getName()).log(Level.SEVERE, null, ex);
            }
            }catch(IOException e){
                e.printStackTrace();
        }
    }
    
    public void showMakeAppointment() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/AppointmentFXML.fxml"));
            BorderPane makeAppt = (BorderPane) loader.load();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Add Appointment");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(apptStage);
            Scene scene = new Scene(makeAppt);
            secondaryStage.setScene(scene);
            
            AppointmentFXMLController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void showAddCustomer() {
       try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/CustomerAddFXML.fxml"));
            BorderPane custAdd = (BorderPane) loader.load();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Add Customer");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(apptStage);
            Scene scene = new Scene(custAdd);
            secondaryStage.setScene(scene);
            
            CustomerAddFXMLController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch(Exception e){
           e.printStackTrace(); 
        } 
    }
    
    public void showModCustomer() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/CustomerModFXML.fxml"));
            BorderPane custMod = (BorderPane) loader.load();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Modify Customer");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(apptStage);
            Scene scene = new Scene(custMod);
            secondaryStage.setScene(scene);
            
            CustomerModFXMLController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch(Exception e){
           e.printStackTrace(); 
        }
    }
    
    public void showModifyAppt() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/ModifyAppointmentFXML.fxml"));
            BorderPane modAppt = (BorderPane) loader.load();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Modify Appointment");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(apptStage);
            Scene scene = new Scene(modAppt);
            secondaryStage.setScene(scene);
            
            ModifyAppointmentFXMLController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch(Exception e){
           e.printStackTrace(); 
        }
    }
    
    public void showGenerateReport() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/GenerateReportsFXML.fxml"));
            BorderPane genReports = (BorderPane) loader.load();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Alert");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(apptStage);
            Scene scene = new Scene(genReports);
            secondaryStage.setScene(scene);
            
            GenerateReportsFXMLController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch(Exception e){
           e.printStackTrace(); 
        }
    }
    
    public void showConsultantReport() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/ConsultantReportFXML.fxml"));
            BorderPane reports = (BorderPane) loader.load();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Report - Consultant Appointments");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(apptStage);
            Scene scene = new Scene(reports);
            secondaryStage.setScene(scene);
            
            ConsultantReportFXMLController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch(Exception e){
           e.printStackTrace(); 
        }
    }
    
    public void showTypeByMonthReport() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/TypeByMonthFXML.fxml"));
            BorderPane reports = (BorderPane) loader.load();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Report - Appointment Types By Month");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(apptStage);
            Scene scene = new Scene(reports);
            secondaryStage.setScene(scene);
            
            TypeByMonthFXMLController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch(Exception e){
           e.printStackTrace(); 
        }
    }
    
    public void showTotApptsByMonthReport() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppointmentSystemMain.class.getResource("controller/ConsultantTotalApptsByMonthFXML.fxml"));
            BorderPane reports = (BorderPane) loader.load();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Report - Total Appointments By Month");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(apptStage);
            Scene scene = new Scene(reports);
            secondaryStage.setScene(scene);
            
            ConsultantTotalApptsByMonthFXMLController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch(Exception e){
           e.printStackTrace(); 
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * @return the ApptStage
     */
    public Stage getApptStage() {
        return apptStage;
    }

    /**
     * @return the MainPane
     */
    public BorderPane getMainPane() {
        return mainPane;
    }

    /**
     * @return the logInStage
     */
    public Stage getLogInStage() {
        return logInStage;
    }
    
}
