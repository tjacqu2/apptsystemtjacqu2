/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;


/**
 *
 * @author tomjacques
 */
public class DBAccess {
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String db = "U04MYK";
    private static final String url = "jdbc:mysql://52.206.157.109/" + db;
    private static final String user = "U04MYK";
    private static final String pass = "53688282782";//"53688282782"
    public static final String getPassword = "SELECT password FROM user WHERE userName = ?";
    public static final String getCountryID = "SELECT MAX(countryId) AS maxCountryId FROM country";
    public static Customer tempCust;
    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss a";
    public static Connection dbConnection = null;
    public static PreparedStatement ps = null;
    //refactor for more general database retrieval
    public static String checkApplicationCredentials(String statementSQL, String attribute, String identifier) throws SQLException {
        //dbConnection = null;
        //ps = null;
        String string = null; 
        try{
            //dbConnection = dbConnectionOpen();
            ps = dbConnection.prepareStatement(statementSQL);
            ps.setString(1, identifier);
            ResultSet rSet = ps.executeQuery();
            while(rSet.next()) {
                string = rSet.getString(attribute);   
            }
            //return string;
        }catch(SQLException e){
            dbConnectionClose(ps, dbConnection);
            System.out.println("Database Closed!");
            System.out.println("checkApplicationCredentials " + e.getMessage());
        }
        return string;//password field cannot be blank
    }
    /*initial opening of DB*/
    public static ObservableList<Customer> getDBCustomerList() throws SQLException{//ID, name, phone only
        String name = "";
        String phone = "";
        int ID = 0;
        //Connection dbConnection = null;
        //PreparedStatement ps = null; 
        try{
            Customer.getCustomerList().clear();
            attachZoneIdToMadeAppt();
            dbConnection = dbConnectionOpen();//open DB here
            ps = dbConnection.prepareStatement("SELECT customerName, customerId FROM customer");
            ResultSet rSet = ps.executeQuery();
            ps = dbConnection.prepareStatement("SELECT phone FROM address");
            ResultSet rSet2 = ps.executeQuery();
            while(rSet.next()){
                name = rSet.getString("customerName");
                ID = rSet.getInt("customerId");
                if(rSet2.next()){
                    phone = rSet2.getString("phone");
                }
                Customer cust = new Customer();
                cust.setCustID(ID);
                cust.setCustName(name);
                cust.setCustPhone(phone);
                Customer.getCustomerList().add(cust);
            }
        }catch(SQLException e){
            dbConnectionClose(ps, dbConnection);
            System.out.println("getDBCustomerList " + e.getMessage()); 
        }
        return Customer.getCustomerList();  
    }
    
    public static Customer getSingleCustomerFromDB(int customerId) throws SQLException{
        //Connection dbConnection = null;
        //PreparedStatement ps = null;
        String tempCountry = "";
        String tempCity = "";
        String tempAddress = "";
        String tempAddress2 = "";
        String tempZip = "";
        String tempPhone = "";
        int tempID = 0;
        String tempCustomerName = "";
        try{
            //dbConnection = dbConnectionOpen();
            ps = dbConnection.prepareStatement("SELECT country FROM country WHERE countryId = ?");
            ps.setInt(1, customerId);
            ResultSet rSet1 = ps.executeQuery();
            ps = dbConnection.prepareStatement("SELECT city FROM city WHERE cityId = ?");
            ps.setInt(1, customerId);
            ResultSet rSet2 = ps.executeQuery();
            ps = dbConnection.prepareStatement("SELECT address, address2, postalCode, phone FROM address WHERE addressId = ?");
            ps.setInt(1, customerId);
            ResultSet rSet3 = ps.executeQuery();
            ps = dbConnection.prepareStatement("SELECT customerId, customerName FROM customer WHERE customerId = ?");
            ps.setInt(1, customerId);
            ResultSet rSet4 = ps.executeQuery();
            while(rSet1.next()) {
                tempCountry = rSet1.getString("country");
            }
            while(rSet2.next()) {
                tempCity = rSet2.getString("city");
            }
            while(rSet3.next()) {
                tempAddress = rSet3.getString("address");
                tempAddress2 = rSet3.getString("address2");
                tempZip = rSet3.getString("postalCode");
                tempPhone = rSet3.getString("phone");
            }
            while(rSet4.next()) {
                tempID = rSet4.getInt("customerId");
                tempCustomerName = rSet4.getString("customerName");
            }
            
            tempCust = new Customer();
            tempCust.setCustID(tempID);
            tempCust.setCustName(tempCustomerName);
            tempCust.setCustAddress(tempAddress);
            tempCust.setCustAddress2(tempAddress2);
            tempCust.setCustCity(tempCity);
            tempCust.setCustCountry(tempCountry);
            tempCust.setCustZip(tempZip);
            tempCust.setCustPhone(tempPhone);

        }catch(Exception e) {
            dbConnectionClose(ps, dbConnection);
            System.out.println("getSingleCustomerFromDB " + e.getMessage());
        }
        return tempCust;
    }
    
    public static void addAppointment(String customerName, String contact, String title, String date, String customerID, String apptType, String apptStart, String apptEnd, 
            String createdBy, String lastUpdateBy) throws SQLException{
        
        Supplier<LocalDateTime> time = () -> LocalDateTime.now();
        Timestamp t = Timestamp.valueOf(time.get());//converts local date time to timestamp
        
        //LocalDateTime ldt = LocalDateTime.now();
        //Timestamp t = Timestamp.valueOf(ldt);//converts local date time to timestamp
        
        int fk = 0;
         //convert apptStart and apptStop from standard to military time
        String timeStartMilitary  = standardToMilitaryTime(apptStart);
        String timeEndMilitary = standardToMilitaryTime(apptEnd);
        //append date and time for apptStart and apptEnd
        String startDateTime = date + " " + timeStartMilitary;
        String endDateTime = date + " " + timeEndMilitary;
        
        Timestamp timeStart = Timestamp.valueOf(startDateTime);
        Timestamp timeEnd = Timestamp.valueOf(endDateTime);
        
        String statementSQLappt = "INSERT INTO appointment"
                + "(customerId, title, description, location, contact, url, start, end, createDate, createdBy, lastUpdate, lastUpdateBy) VALUES"
                + "(?,?,?,?,?,?,?,?,?,?,?,?)";
        //try{
            String zoneId = attachZoneIdToMadeAppt();//get ZonedDateTime and put zone id in DB appointment location column
            //dbConnection = dbConnectionOpen();
            ps = dbConnection.prepareStatement("SELECT customerId FROM customer WHERE customerName = ?");
            ps.setString(1, customerName);
            ResultSet rSet = ps.executeQuery();
            while(rSet.next()) {
               fk = rSet.getInt("customerId");
           }
            //appointment table
            ps = dbConnection.prepareStatement(statementSQLappt);
            ps.setInt(1, fk);//customerID
            ps.setString(2, title);//title
            ps.setString(3, apptType);//appointment type
            ps.setString(4, zoneId);//location
            ps.setString(5, contact);//contact
            ps.setString(6, "NA");//url
            ps.setTimestamp(7, timeStart);//appointment start time
            ps.setTimestamp(8, timeEnd);//appointment end time
            ps.setTimestamp(9, t);//create date
            ps.setString(10, createdBy);//created by
            ps.setTimestamp(11, t);//last update
            ps.setString(12, lastUpdateBy);//last update by
            ps.executeUpdate();
        
    }
    
    public static void updateAppointment(int apptId, String contact, String title, String date, String apptStart, String apptEnd, String apptType, String lastUpdateBy) throws SQLException{ 
        Supplier<LocalDateTime> time = () -> LocalDateTime.now();
        Timestamp t = Timestamp.valueOf(time.get());//converts local date time to timestamp
        //Connection dbConnection = null;
        //PreparedStatement ps = null;
        int fk = 0;
         //convert apptStart and apptStop from standard to military time
        String timeStartMilitary  = standardToMilitaryTime(apptStart);
        String timeEndMilitary = standardToMilitaryTime(apptEnd);
        //append date and time for apptStart and apptEnd
        String startDateTime = date + " " + timeStartMilitary;
        String endDateTime = date + " " + timeEndMilitary;
        Timestamp timeStart = Timestamp.valueOf(startDateTime);
        Timestamp timeEnd = Timestamp.valueOf(endDateTime);
        
        String statementSQLappt = "UPDATE appointment SET title = ?, description = ?, location = ?, contact = ?, start = ?, end = ?, lastUpdate = ?, lastUpdateBy = ?"
                + "WHERE appointmentId = ?";
        //try{
            String zoneId = attachZoneIdToMadeAppt();//get ZonedDateTime and put zone id in db appointment location column
            //appointment table
            ps = dbConnection.prepareStatement(statementSQLappt);
            ps.setString(1, title);//title
            ps.setString(2, apptType);//appointment type
            ps.setString(3, zoneId);//location
            ps.setString(4, contact);//contact            
            ps.setTimestamp(5, timeStart);//appointment start time
            ps.setTimestamp(6, timeEnd);//appointment end time
            ps.setTimestamp(7, t);//last update
            ps.setString(8, lastUpdateBy);//last update by
            ps.setInt(9, apptId);
            ps.executeUpdate();
    }    
    
    public static void deleteAppointment(int apptId) throws SQLException{
        try{
            ps = dbConnection.prepareStatement("DELETE FROM appointment WHERE appointmentId = ?");
            ps.setInt(1, apptId);
            ps.executeUpdate();
        }catch (SQLException e){
            dbConnectionClose(ps, dbConnection);
            System.out.println("deleteAppointment" + e.getMessage());
        }
    } 
    
    public static void addCustomerRecord(String customerName, int active, String address, String address2, 
            String postalCode, String phone, String city, String country, String createdBy, String lastUpdateBy) throws SQLException{
    //public void addCustomerToDB(String table, int custID, String name, int active, String createDate, String createdBy, String lastUpdate, String lastUpdateBy) throws SQLException{
        //Connection dbConnection = null;
        //PreparedStatement ps = null;
        int fk = 0;
        String statementSQLcountry = "INSERT INTO country"
                + "(country, createDate, createdBy, lastUpdate, lastUpdateBy) VALUES"
                + "(?,?,?,?,?)";
        //get countryID for all foreign keys
        
        
        String statementSQLcity = "INSERT INTO city"
                + "(city, countryId, createDate, createdBy, lastUpdate, lastUpdateBy) VALUES"
                + "(?,?,?,?,?,?)";

        
        String statementSQLaddress = "INSERT INTO address"
                + "(address, address2, cityId, postalCode, phone, createDate, createdBy, lastUpdate, lastUpdateBy) VALUES"
                + "(?,?,?,?,?,?,?,?,?)";
        
        
        String statementSQLcustomer = "INSERT INTO customer"
                + "(customerName, addressId, active, createDate, createdBy, lastUpdate, lastUpdateBy) VALUES"
                + "(?,?,?,?,?,?,?)";
        try{
            LocalDateTime ldt = LocalDateTime.now();
            Timestamp t = Timestamp.valueOf(ldt);
            //dbConnection = dbConnectionOpen();
            //country table
            ps = dbConnection.prepareStatement(statementSQLcountry);
            ps.setString(1, country);
            ps.setTimestamp(2, t);
            ps.setString(3, createdBy);
            ps.setTimestamp(4, t);
            ps.setString(5, lastUpdateBy);
            ps.executeUpdate();
            
            ps = dbConnection.prepareStatement("SELECT MAX(countryId) AS maxCountryId FROM country");
            ResultSet rSet = ps.executeQuery();
            while(rSet.next()) {
               fk = rSet.getInt("maxCountryId");   
            }
            //city table
            ps = dbConnection.prepareStatement(statementSQLcity);
            ps.setString(1, city);
            ps.setInt(2, fk);
            ps.setTimestamp(3, t);
            ps.setString(4, createdBy);
            ps.setTimestamp(5, t);
            ps.setString(6, lastUpdateBy);
            ps.executeUpdate();
            //address table
            ps = dbConnection.prepareStatement(statementSQLaddress);
            ps.setString(1, address);
            ps.setString(2, address2);
            ps.setInt(3, fk);
            ps.setString(4, postalCode);
            ps.setString(5, phone);
            ps.setTimestamp(6, t);
            ps.setString(7, createdBy);
            ps.setTimestamp(8, t);
            ps.setString(9, lastUpdateBy);
            ps.executeUpdate();
            //customer table
            ps = dbConnection.prepareStatement(statementSQLcustomer);
            ps.setString(1, customerName);
            ps.setInt(2, fk);
            ps.setInt(3, active);
            ps.setTimestamp(4, t);
            ps.setString(5, createdBy);
            ps.setTimestamp(6, t);
            ps.setString(7, lastUpdateBy);
            ps.executeUpdate();
        }catch(SQLException e){
            dbConnectionClose(ps, dbConnection);
            System.out.println("addCustomerRecord" + e.getMessage());
        }
    }
    
    public static void modifyCustomerRecord(int ID, String customerName, String customerAddress, String customerAddress2, 
            String customerCity, String customerZip, String customerCountry, String customerPhone, String lastUpdateBy, int active
    /*lastUpdateTimeCurrentTimestamp*/) throws SQLException {
        //Connection dbConnection = null;
        //PreparedStatement ps = null;
        int fk = 0;
        String statementSQLcountry = "UPDATE country SET country = ?, lastUpdate = ?, lastUpdateBy = ? WHERE countryId = ?";
        String statementSQLcity = "UPDATE city SET city = ?, lastUpdate = ?, lastUpdateBy = ? WHERE cityId = ?";
        String statementSQLaddress = "UPDATE address SET address = ?, address2 = ?, postalCode = ?, phone = ?, lastUpdate = ?, lastUpdateBy = ? WHERE addressId = ?";
        String statementSQLcustomer = "UPDATE customer SET customerName = ?, active = ?, lastUpdate = ?, lastUpdateBy = ? WHERE customerId = ?";
        try{
            LocalDateTime ldt = LocalDateTime.now();
            Timestamp t = Timestamp.valueOf(ldt);
            //dbConnection = dbConnectionOpen();
            //country table
            ps = dbConnection.prepareStatement(statementSQLcountry);
            ps.setInt(4, ID);
            ps.setString(1, customerCountry);
            ps.setTimestamp(2, t);//last update
            ps.setString(3, lastUpdateBy);
            ps.executeUpdate();
            //city table
            ps = dbConnection.prepareStatement(statementSQLcity);
            ps.setInt(4, ID);
            ps.setString(1, customerCity);
            ps.setTimestamp(2, t);
            ps.setString(3, lastUpdateBy);
            ps.executeUpdate();
            //address table
            ps = dbConnection.prepareStatement(statementSQLaddress);
            ps.setInt(7, ID);
            ps.setString(1, customerAddress);
            ps.setString(2, customerAddress2);
            ps.setString(3, customerZip);
            ps.setString(4, customerPhone);
            ps.setTimestamp(5, t);
            ps.setString(6, lastUpdateBy);
            ps.executeUpdate();
            //customer table
            ps = dbConnection.prepareStatement(statementSQLcustomer);
            ps.setInt(5, ID);
            ps.setString(1, customerName);
            ps.setInt(2, active);
            ps.setTimestamp(3, t);
            ps.setString(4, lastUpdateBy);
            ps.executeUpdate();
        }catch(SQLException e){
            dbConnectionClose(ps, dbConnection);
            System.out.println("modifyCustomerRecord" + e.getMessage());
        }
    }
    
    public static void deleteCustomerRecord(int custId) throws SQLException{
        try{
            ps = dbConnection.prepareStatement("DELETE FROM customer WHERE customerId = ?");
            ps.setInt(1, custId);
            ps.executeUpdate();
            ps = dbConnection.prepareStatement("DELETE FROM appointment WHERE customerId = ?");
            ps.setInt(1, custId);
            ps.executeUpdate();
        }catch (SQLException e){
            dbConnectionClose(ps, dbConnection);
            System.out.println("deleteCustomerRecord" + e.getMessage());
        }
    }
    
    
    
    public static void getConsultantScheduleDataAll() throws SQLException{
        int tempApptID = 0;
        int tempCustID = 0;
        String tempApptType = "";
        String tempConsultant = "";
        String tempCustomer = "";
        Timestamp tempStart;
        Timestamp tempEnd;
        String tempLocation;
        String tempPhone = "";
        String tempContact = "";
        String tempTitle = "";
        Appointments.getApptList().clear();
        //Connection dbConnection = null;
        //PreparedStatement ps = null;
        try{
            //dbConnection = dbConnectionOpen();
            //ps = dbConnection.prepareStatement("SELECT appointmentId, customerId, description, start, end, createdBy FROM appointment");
            ps = dbConnection.prepareStatement("SELECT appointmentId, customerId, description, start, end, location, createdBy, contact, title FROM appointment");//new
            
            ResultSet rSet1 = ps.executeQuery();
     
            while(rSet1.next()){
                tempApptID = rSet1.getInt("appointmentId");
                tempCustID = rSet1.getInt("customerId");
                tempApptType = rSet1.getString("description");
                tempStart = rSet1.getTimestamp("start");
                tempEnd = rSet1.getTimestamp("end");
                tempLocation = rSet1.getString("location");
                tempConsultant = rSet1.getString("createdBy");
                tempContact = rSet1.getString("contact");
                tempTitle = rSet1.getString("title");
                //tempConsultant = rSet1.getString("createdBy");//removed for new
                ps = dbConnection.prepareStatement("SELECT phone FROM address WHERE addressId = ?");
                ps.setInt(1, tempCustID);
                ResultSet rSet2 = ps.executeQuery();
                ps = dbConnection.prepareStatement("SELECT customerName FROM customer WHERE addressId = ?");
                ps.setInt(1, tempCustID);
                ResultSet rSet3 = ps.executeQuery();
                //do timestamp conversion here
  
                //end timestamp conversion
                if(rSet2.next()){
                    tempPhone = rSet2.getString("phone");
                }
                if(rSet3.next()){
                    tempCustomer = rSet3.getString("customerName");
                }
                String ts, te;
                    String timeOnlySt, timeOnlyEnd;
                    String dateOnly;
                    Appointments tempApp = new Appointments();                
                    //convert timestamp to string
                    ts = tempApp.setTimeStart(tempStart);//convert timestamp to string
                    te = tempApp.setTimeEnd(tempEnd);//convert timestamp to string
                    
                    ts = TimeConversionOriginalLocationToNewLocation(ts, tempLocation);
                    te = TimeConversionOriginalLocationToNewLocation(te, tempLocation);
                    
                    //convert military to standard time
                    timeOnlySt = militaryToStandardTime(ts);
                    timeOnlyEnd = militaryToStandardTime(te);
                    dateOnly = returnDateFromTimestamp(ts);
                    
                    //break out date separately
                    tempApp.setTimeStartSt(timeOnlySt);//convert timestamp string to SimpleProperty value for cellFactory
                    tempApp.setTimeEndSt(timeOnlyEnd);//convert timestamp string to SimpleProperty value for cellFactory
                    tempApp.setDate(dateOnly);
                    tempApp.setApptId(tempApptID);
                    tempApp.setApptType(tempApptType);
                    tempApp.setCustomer(tempCustomer);
                    tempApp.setPhone(tempPhone);
                    tempApp.setConsultant(tempConsultant);
                    tempApp.setCustomerId(tempCustID);
                    tempApp.setContact(tempContact);
                    tempApp.setTitle(tempTitle);
                    Appointments.getApptList().add(tempApp);
            }
        }catch(SQLException e) {
            dbConnectionClose(ps, dbConnection);
            System.out.println("getConsultantScheduleDataAll" + e.getMessage());
        }
        //return Appointments.getApptList();
    }
    
    public static void getConsultantScheduleData(String userName) throws SQLException{
        int tempApptID = 0;
        int tempCustID = 0;
        String tempApptType = "";
        String tempConsultant = "";
        String tempCustomer = "";
        Timestamp tempStart;
        Timestamp tempEnd;
        String tempLocation;
        String tempPhone = "";
        String tempContact = "";
        String tempTitle = "";
        Appointments.getApptList().clear();
        //Connection dbConnection = null;
        //PreparedStatement ps = null;
        try{
            //dbConnection = dbConnectionOpen();
            //ps = dbConnection.prepareStatement("SELECT appointmentId, customerId, description, start, end, createdBy FROM appointment");
            ps = dbConnection.prepareStatement("SELECT appointmentId, customerId, description, start, end, location , contact, title FROM appointment WHERE createdBy = ?");//new
            ps.setString(1, userName);//new
            ResultSet rSet1 = ps.executeQuery();
     
            while(rSet1.next()){
                tempApptID = rSet1.getInt("appointmentId");
                tempCustID = rSet1.getInt("customerId");
                tempApptType = rSet1.getString("description");
                tempStart = rSet1.getTimestamp("start");
                tempEnd = rSet1.getTimestamp("end");
                tempLocation = rSet1.getString("location");
                tempContact = rSet1.getString("contact");
                tempTitle = rSet1.getString("title");
                //tempConsultant = rSet1.getString("createdBy");//removed for new
                ps = dbConnection.prepareStatement("SELECT phone FROM address WHERE addressId = ?");
                ps.setInt(1, tempCustID);
                ResultSet rSet2 = ps.executeQuery();
                ps = dbConnection.prepareStatement("SELECT customerName FROM customer WHERE addressId = ?");
                ps.setInt(1, tempCustID);
                ResultSet rSet3 = ps.executeQuery();
                //do timestamp conversion here
  
                //end timestamp conversion
                if(rSet2.next()){
                    tempPhone = rSet2.getString("phone");
                }
                if(rSet3.next()){
                    tempCustomer = rSet3.getString("customerName");
                }
                
                    String ts, te;
                    String timeOnlySt, timeOnlyEnd;
                    String dateOnly;
                    Appointments tempApp = new Appointments();                
                    //convert timestamp to string
                    ts = tempApp.setTimeStart(tempStart);//convert timestamp to string
                    te = tempApp.setTimeEnd(tempEnd);//convert timestamp to string
                    //tempLocation = "America/Los_Angeles";//until new appointments added
                    
                    ts = TimeConversionOriginalLocationToNewLocation(ts, tempLocation);
                    te = TimeConversionOriginalLocationToNewLocation(te, tempLocation);
                    
                    //convert military to standard time
                    timeOnlySt = militaryToStandardTime(ts);
                    timeOnlyEnd = militaryToStandardTime(te);
                    dateOnly = returnDateFromTimestamp(ts);
                    
                    //break out date separately
                    tempApp.setTimeStart(tempStart); 
                    tempApp.setTimeStartSt(timeOnlySt);//convert timestamp string to SimpleProperty value for cellFactory
                    tempApp.setTimeEndSt(timeOnlyEnd);//convert timestamp string to SimpleProperty value for cellFactory
                    tempApp.setDate(dateOnly);
                    tempApp.setApptId(tempApptID);
                    tempApp.setApptType(tempApptType);
                    tempApp.setCustomer(tempCustomer);
                    tempApp.setCustomerId(tempCustID);
                    tempApp.setPhone(tempPhone);
                    tempApp.setContact(tempContact);
                    tempApp.setTitle(tempTitle);
                    Appointments.getApptList().add(tempApp);
            }
       }catch(SQLException e) {
            dbConnectionClose(ps, dbConnection);
            System.out.println("getConsultantScheduleData" + e.getMessage());
       }
       //return Appointments.getApptList();
    }
    //weekly
    public static void getConsultantScheduleDataWeekly(String userName, int week) throws SQLException{
        int tempApptID = 0;
        int tempCustID = 0;
        String tempApptType = "";
        String tempConsultant = "";
        String tempCustomer = "";
        Timestamp tempStart;
        Timestamp tempEnd;
        String tempLocation;
        String tempPhone = "";
        String tempContact = "";
        String tempTitle = "";
        Appointments.getApptListWeekly().clear();
        //Connection dbConnection = null;
        //PreparedStatement ps = null;
        try{
            //dbConnection = dbConnectionOpen();
            //ps = dbConnection.prepareStatement("SELECT appointmentId, customerId, description, start, end, createdBy FROM appointment");
            ps = dbConnection.prepareStatement("SELECT appointmentId, customerId, description, start, end, location , contact, title FROM appointment WHERE(createdBy = ? AND WEEK(start) = ?)");//new
            ps.setString(1, userName);//new
            ps.setInt(2, week - 1);
            ResultSet rSet1 = ps.executeQuery();
     
            while(rSet1.next()){
                tempApptID = rSet1.getInt("appointmentId");
                tempCustID = rSet1.getInt("customerId");
                tempApptType = rSet1.getString("description");
                tempStart = rSet1.getTimestamp("start");
                tempEnd = rSet1.getTimestamp("end");
                tempLocation = rSet1.getString("location");
                tempContact = rSet1.getString("contact");
                tempTitle = rSet1.getString("title");
                //tempConsultant = rSet1.getString("createdBy");//removed for new
                ps = dbConnection.prepareStatement("SELECT phone FROM address WHERE addressId = ?");
                ps.setInt(1, tempCustID);
                ResultSet rSet2 = ps.executeQuery();
                ps = dbConnection.prepareStatement("SELECT customerName FROM customer WHERE addressId = ?");
                ps.setInt(1, tempCustID);
                ResultSet rSet3 = ps.executeQuery();
                //do timestamp conversion here
  
                //end timestamp conversion
                if(rSet2.next()){
                    tempPhone = rSet2.getString("phone");
                }
                if(rSet3.next()){
                    tempCustomer = rSet3.getString("customerName");
                }
                    String ts, te;
                    String timeOnlySt, timeOnlyEnd;
                    String dateOnly;
                    Appointments tempApp = new Appointments();                
                    //convert timestamp to string
                    ts = tempApp.setTimeStart(tempStart);//convert timestamp to string
                    te = tempApp.setTimeEnd(tempEnd);//convert timestamp to string
                    
                    ts = TimeConversionOriginalLocationToNewLocation(ts, tempLocation);
                    te = TimeConversionOriginalLocationToNewLocation(te, tempLocation);
                    
                    //convert military to standard time
                    timeOnlySt = militaryToStandardTime(ts);
                    timeOnlyEnd = militaryToStandardTime(te);
                    dateOnly = returnDateFromTimestamp(ts);
                    
                    //break out date separately
                    tempApp.setTimeStartSt(timeOnlySt);//convert timestamp string to SimpleProperty value for cellFactory
                    tempApp.setTimeEndSt(timeOnlyEnd);//convert timestamp string to SimpleProperty value for cellFactory
                    tempApp.setDate(dateOnly);
                    tempApp.setApptId(tempApptID);
                    tempApp.setApptType(tempApptType);
                    tempApp.setCustomer(tempCustomer);
                    tempApp.setCustomerId(tempCustID);
                    tempApp.setPhone(tempPhone);
                    tempApp.setContact(tempContact);
                    tempApp.setTitle(tempTitle);
                    Appointments.getApptListWeekly().add(tempApp);
            }
        }catch(SQLException e) {
             dbConnectionClose(ps, dbConnection);
             System.out.println("getConsultantScheduleDataWeekly" + e.getMessage());
        }
        //return Appointments.getApptListWeekly();
    }
    
    public static void getConsultantScheduleDataMonthly(String userName, int month, String year ) throws SQLException{
        int tempApptID = 0;
        int tempCustID = 0;
        String tempApptType = "";
        String tempConsultant = "";
        String tempCustomer = "";
        Timestamp tempStart;
        Timestamp tempEnd;
        String tempLocation;
        String tempPhone = "";
        String tempContact = "";
        String tempTitle = "";
        Appointments.getApptListMonthly().clear();
        //Connection dbConnection = null;
        //PreparedStatement ps = null;
        try{
            //dbConnection = dbConnectionOpen();
            //ps = dbConnection.prepareStatement("SELECT appointmentId, customerId, description, start, end, createdBy FROM appointment");
            ps = dbConnection.prepareStatement("SELECT appointmentId, customerId, description, start, end, location , contact, title FROM appointment WHERE(createdBy = ? AND MONTH(start) = ? AND YEAR(start) = ?)");//new
            ps.setString(1, userName);//new
            ps.setInt(2, month);
            ps.setString(3, year);
            ResultSet rSet1 = ps.executeQuery();
     
            while(rSet1.next()){
                tempApptID = rSet1.getInt("appointmentId");
                tempCustID = rSet1.getInt("customerId");
                tempApptType = rSet1.getString("description");
                tempStart = rSet1.getTimestamp("start");
                tempEnd = rSet1.getTimestamp("end");
                tempLocation = rSet1.getString("location");
                tempContact = rSet1.getString("contact");
                tempTitle = rSet1.getString("title");
                //tempConsultant = rSet1.getString("createdBy");//removed for new
                ps = dbConnection.prepareStatement("SELECT phone FROM address WHERE addressId = ?");
                ps.setInt(1, tempCustID);
                ResultSet rSet2 = ps.executeQuery();
                ps = dbConnection.prepareStatement("SELECT customerName FROM customer WHERE addressId = ?");
                ps.setInt(1, tempCustID);
                ResultSet rSet3 = ps.executeQuery();
                //do timestamp conversion here
  
                //end timestamp conversion
                if(rSet2.next()){
                    tempPhone = rSet2.getString("phone");
                }
                if(rSet3.next()){
                    tempCustomer = rSet3.getString("customerName");
                }
                    String ts, te;
                    String timeOnlySt, timeOnlyEnd;
                    String dateOnly;
                    Appointments tempApp = new Appointments();
                    
                    ts = tempApp.setTimeStart(tempStart);
                    te = tempApp.setTimeEnd(tempEnd);
                    
                    ts = TimeConversionOriginalLocationToNewLocation(ts, tempLocation);
                    te = TimeConversionOriginalLocationToNewLocation(te, tempLocation);
                    //convert military to standard time
                    timeOnlySt = militaryToStandardTime(ts);
                    timeOnlyEnd = militaryToStandardTime(te);
                    dateOnly = returnDateFromTimestamp(ts);
                    //dateOnly = returnDateFromTimestamp(ts);
                    //break out date separately
                    
                    tempApp.setTimeStartSt(timeOnlySt);//convert timestamp to string to SimpleProperty value for cellFactory
                    tempApp.setTimeEndSt(timeOnlyEnd);//convert timestamp to string to SimpleProperty value for cellFactory
                    tempApp.setDate(dateOnly);
                    tempApp.setApptId(tempApptID);
                    tempApp.setApptType(tempApptType);
                    tempApp.setCustomer(tempCustomer);
                    tempApp.setCustomerId(tempCustID);
                    tempApp.setPhone(tempPhone);
                    tempApp.setContact(tempContact);
                    tempApp.setTitle(tempTitle);
                    Appointments.getApptListMonthly().add(tempApp);
            }
        }catch(SQLException e) {
             dbConnectionClose(ps, dbConnection);
             System.out.println("getConsultantScheduleDataMonthly" + e.getMessage());
        }
        //return Appointments.getApptListMonthly();
    }
    
    private static String attachZoneIdToMadeAppt() {
        String zoneId = ZonedDateTime.now().toString().substring(30, ZonedDateTime.now().toString().length() -1);
        return zoneId;
    }    
    
    private static String TimeConversionOriginalLocationToNewLocation(String timeDate, String originalZoneId) {
        String newTime;
        String sdfString  = timeDate;
        LocalDateTime ldt = LocalDateTime.parse(sdfString, DateTimeFormatter.ofPattern(DATE_FORMAT));
        /*This is the time zone where the appointment was made*/
        ZoneId apptMadeZone = ZoneId.of(originalZoneId);
        ZonedDateTime ozId = ldt.atZone(apptMadeZone);
        /*This is the time zone the user is currently in*/
        ZoneId currentTimeZone = ZoneId.of(ZonedDateTime.now().toString().substring(30, ZonedDateTime.now().toString().length() -1));//change time zone here to simulate moving to new time zone(ie: America/New_York)
        ZonedDateTime convertedTime = ozId.withZoneSameInstant(currentTimeZone);
        /*This is where time is converted*/
        DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
        newTime = format.format(convertedTime);
        return newTime;
    }
    
    private static String returnDateFromTimestamp(String time){
        String reformattedDate = "";
        try {
            String tempDate = time.substring(0, 10);
            SimpleDateFormat inputDate = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat outputDate = new SimpleDateFormat("MM-dd-yyyy");
            Date date = inputDate.parse(tempDate);
            reformattedDate = outputDate.format(date);
        } catch (ParseException ex) {
            Logger.getLogger(DBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reformattedDate;
    }
    
    private static String militaryToStandardTime(String time) {
        String standardTime = "";
        String temp1 = time.substring(0, 11);
        String temp2 = time.substring(13);
        String nonConvertedTime = time.substring(11);
        String convert = ""; 
        int hour = Integer.parseInt(time.substring(11,13));
            if (hour >= 13 && hour <= 24) {
                hour = hour - 12;
                if(hour >= 1 || hour <= 9){
                    convert = "0" + Integer.toString(hour);
                }else{
                convert = Integer.toString(hour);
                //standardTime = convert + temp2;
                }
                standardTime = convert + temp2;
            }else{
                standardTime = nonConvertedTime;
            }
            return standardTime;
    }
    
    public static String standardToMilitaryTime(String time) {
        String militaryTime = "";
        String convert = "";
        int hour = Integer.parseInt(time.substring(0,2));
        if(Character.toString(time.charAt(6)).matches("[P]")) {
            if(hour >= 1 && hour < 12){
                hour += 12;
                convert = Integer.toString(hour);
            }else{
                convert = Integer.toString(hour);
            }
        }else{
            if(hour == 12) {
                hour = 0;
                convert = "0" + Integer.toString(hour);
            }
            else if(hour >= 1 && hour <= 9){
                convert = "0" + Integer.toString(hour);
            }else{
                convert = Integer.toString(hour);
            }
        }
        militaryTime = convert + time.substring(2, 5) + ":00";
        return militaryTime;
    }
    
    public static LocalTime timeStringToLocalTimeMilitary(String time) {
       String tempStart = standardToMilitaryTime(time);
       tempStart = tempStart.substring(0, 5);
       LocalTime timeChk = LocalTime.parse(tempStart);//string to LocalTime
       return timeChk;
    }
    
    private static Connection dbConnectionOpen() {
        //Connection dbConnection = null;
        try{
            Class.forName(driver);
        
            dbConnection = DriverManager.getConnection(url, user, pass);
            return dbConnection;
        }catch(ClassNotFoundException cnf){
           System.out.println(cnf.getMessage());        
        }catch(SQLException e){
           System.out.println(e.getMessage());
        }
        return dbConnection;
    }
    
    public static void dbConnectionClose(PreparedStatement statement, Connection conn) throws SQLException {
        try{
            if(statement != null) {
                statement.close();
            }
            if(conn != null) {
                conn.close();
                System.out.println("Database Closed!");
            }
        }catch(SQLException e){
                System.out.println(e.getMessage());
            }
    }
    
}
