/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author tomjacques
 */
public class Report {
    
    private StringProperty month;
    private StringProperty type;
    private IntegerProperty total;
    private StringProperty consultant;
    private static ObservableList<Report> reportList = FXCollections.observableArrayList();
    
    public Report() {
        
    }
    
    public Report(String month, String type, int total) {
        this.month = new SimpleStringProperty(month);
        this.type = new SimpleStringProperty(type);
        this.total = new SimpleIntegerProperty(total);
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month.get();
    }

    /**
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = new SimpleStringProperty(month);
    }

    public StringProperty monthProperty() {
        return month;
    }
    /**
     * @return the type
     */
    public String getType() {
        return type.get();
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = new SimpleStringProperty(type);
    }

    public StringProperty typeProperty() {
        return type;
    }
    /**
     * @return the total
     */
    public int getTotal() {
        return total.get();
    }

    /**
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = new SimpleIntegerProperty(total);
    }

    public IntegerProperty totalProperty() {
        return total;
    }
    
    public void setConsultant(String user) {
        this.consultant = new SimpleStringProperty(user);
    }
    
    public StringProperty consultantProperty() {
        return consultant;
    }
    /**
     * @return the reportList
     */
    public static ObservableList<Report> getReportList() {
        return reportList;
    }

    /**
     * @param reportList the reportList to set
     */
    public void setReportList(ObservableList<Report> reportList) {
        this.reportList = reportList;
    }
}
