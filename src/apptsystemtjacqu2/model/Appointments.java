/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.model;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author tomjacques
 */
public class Appointments {
    
    private static ObservableList<Appointments> apptList = FXCollections.observableArrayList();//appointments date/time, phone
    private static ObservableList<Appointments> apptListWeekly = FXCollections.observableArrayList();//appointments date/time, phone
    private static ObservableList<Appointments> apptListMonthly = FXCollections.observableArrayList();//appointments date/time, phone
    private static ZonedDateTime appointment = ZonedDateTime.now();
    private IntegerProperty customerId;
    private IntegerProperty apptId;
    private StringProperty consultant;
    private StringProperty customer;
    private StringProperty phone;
    private StringProperty contact;
    private StringProperty title;
    private Timestamp timeStart;
    private Timestamp timeEnd;
    private String location;
    private StringProperty apptType;
    private StringProperty timeStartSt;
    private StringProperty timeEndSt;
    private StringProperty date;
    
    
    public Appointments() {
        
    }
    
    public Appointments(int customerId, String consultant, int apptId, String customer, String phone, String contact, String title, Timestamp timeStart, Timestamp timeEnd, String location, String apptType, 
                String timeStartSt, String timeEndSt) {
        this.customerId = new SimpleIntegerProperty(customerId);
        this.consultant = new SimpleStringProperty(consultant);
        this.customer = new SimpleStringProperty(customer);
        this.apptId = new SimpleIntegerProperty(apptId);
        this.phone = new SimpleStringProperty(phone);
        this.contact = new SimpleStringProperty(contact);
        this.title = new SimpleStringProperty(title);
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.apptType = new SimpleStringProperty(apptType);
        
        //this.timeStartSt = new SimpleStringProperty(timeStartSt);
    }

    /**
     * @return the apptList
     */
    public static ObservableList<Appointments> getApptList() {
        return apptList;
    }
    
    /**
     * @param aApptList the apptList to set
     */
    public static void setApptList(ObservableList<Appointments> aApptList) {
        apptList = aApptList;
    }

    /**
     * @return the apptListWeekly
     */
    public static ObservableList<Appointments> getApptListWeekly() {
        return apptListWeekly;
    }

    /**
     * @param aApptListWeekly the apptListWeekly to set
     */
    public static void setApptListWeekly(ObservableList<Appointments> aApptListWeekly) {
        apptListWeekly = aApptListWeekly;
    }

    /**
     * @return the apptListMonthly
     */
    public static ObservableList<Appointments> getApptListMonthly() {
        return apptListMonthly;
    }

    /**
     * @param aApptListMonthly the apptListMonthly to set
     */
    public static void setApptListMonthly(ObservableList<Appointments> aApptListMonthly) {
        apptListMonthly = aApptListMonthly;
    }
    
    /**
     * @return the appointment
     */
    public static ZonedDateTime getAppointment() {
        return appointment;
    }

    /**
     * @param aAppointment the appointment to set
     */
    public static void setAppointment(ZonedDateTime aAppointment) {
        appointment = aAppointment;
    }
    
    /**
     * @return the customerId
     */
    public int getCustomerId() {
        return customerId.get();
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(int customerId) {
        this.customerId = new SimpleIntegerProperty(customerId);
    }
    
    public IntegerProperty customerIdProperty() {
        return customerId;
    }
    /**
     * @return the consultant
     */
    public String getConsultant() {
        return consultant.get();
    }

    /**
     * @param consultant the consultant to set
     */
    public void setConsultant(String consultant) {
        this.consultant = new SimpleStringProperty(consultant);
    }

    public StringProperty consultantProperty() {
        return consultant;
    }
    /**
     * @return the customer
     */
    public String getCustomer() {
        return customer.get();
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(String customer) {
        this.customer = new SimpleStringProperty(customer);
    }

    public StringProperty customerProperty() {
        return customer;
    }
    /**
     * @return the phone
     */
    public String getPhone() {
        return phone.get();
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = new SimpleStringProperty(phone);
    }

    public StringProperty phoneProperty() {
        return phone;
    }
    /**
     * @return the timeStart
     */
    public Timestamp getTimeStart() {
        return timeStart;
    }

    /**
     * @param timeStart the timeStart to set
     */
    public String setTimeStart(Timestamp timeStart) {
        String tempTimeStart;
        this.timeStart = timeStart;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");//old
        //SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss a");//new for time only
        tempTimeStart = dateFormat.format(timeStart);
        return tempTimeStart;
    }
    
    public void setTimeStartSt(String setTimeStart) {//takes return value from setTimeStart
        this.timeStartSt = new SimpleStringProperty(setTimeStart);
    }
    
    public StringProperty timeStartPropertySt() {
        return timeStartSt;
    }
    
    public String getTimeStartSt() {
        return timeStartSt.get();
    }
    
    /**
     * @return the timeEnd
     */
    public Timestamp getTimeEnd() {
        return timeEnd;
    }

    /**
     * @param timeEnd the timeEnd to set
     */
    public String setTimeEnd(Timestamp timeEnd) {
        String tempTimeEnd;
        this.timeEnd = timeEnd;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");//old
        //SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss a");//new for time only
        tempTimeEnd = dateFormat.format(timeEnd);
        return tempTimeEnd;
    }

    public void setTimeEndSt(String setTimeEnd) {//takes return value from setTimeEnd
        this.timeEndSt = new SimpleStringProperty(setTimeEnd);
    }
    
    public String getTimeEndSt() {
        return timeEndSt.get();
    }
    
    public StringProperty timeEndStProperty() {
        return timeEndSt;
    }
    
    /**
     * @return the date
     */
    public String getDate() {
        return date.get();
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = new SimpleStringProperty(date);
    }
    
    public StringProperty dateProperty() {
        return date;
    }
    
    /**
     * @return the apptType
     */
    public String getApptType() {
        return apptType.get();
    }

    /**
     * @param apptType the apptType to set
     */
    public void setApptType(String apptType) {
        this.apptType = new SimpleStringProperty(apptType);
    }
    
    public StringProperty apptTypeProperty() {
        return apptType;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact.get();
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = new SimpleStringProperty(contact);
    }
    
    public StringProperty contactProperty() {
        return contact;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title.get();
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = new SimpleStringProperty(title);
    }
    
    public StringProperty titleProperty() {
        return title;
    }

    /**
     * @return the apptId
     */
    public Integer getApptId() {
        return apptId.get();
    }

    /**
     * @param apptId the apptId to set
     */
    public void setApptId(int apptId) {
        this.apptId = new SimpleIntegerProperty(apptId);
    }
    
    public IntegerProperty apptIdProperty() {
        return apptId;
    }
}
