/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apptsystemtjacqu2.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author tomjacques
 */
public class Customer {
    private IntegerProperty custID;
    private StringProperty custName;
    private StringProperty custAddress;
    private StringProperty custAddress2;
    private StringProperty custCity;
    private StringProperty custZip;
    private StringProperty custCountry;
    private StringProperty custPhone;

    
    private static ObservableList<Customer> customerList = FXCollections.observableArrayList();
    
    public Customer() {
        
    }
    public Customer(int ID, String name, String address, String address2, String city, String zip, String country, String phone) {
        this.custID = new SimpleIntegerProperty(ID);
        this.custName = new SimpleStringProperty(name);
        this.custAddress = new SimpleStringProperty(address);
        this.custAddress2 = new SimpleStringProperty(address2);
        this.custCity = new SimpleStringProperty(city);
        this.custZip = new SimpleStringProperty(zip);
        this.custCountry =  new SimpleStringProperty(country);
        this.custPhone = new SimpleStringProperty(phone);
    }

    /**
     * @return the customerList
     */
    public static ObservableList<Customer> getCustomerList() {
        return customerList;
    }

    /**
     * @return the custID
     */
    public int getCustID() {//good
        return custID.get();
    }

    /**
     * @param custID the custID to set
     */
    public void setCustID(int custID) {
        this.custID = new SimpleIntegerProperty(custID);
    }
    

    public IntegerProperty custIDProperty() {
        return custID;
    }

    /**
     * @return the custName
     */
    public String getCustName() {//good
        return custName.get();
    }

    /**
     * @param custName the custName to set
     */
    public void setCustName(String custName) {
        this.custName = new SimpleStringProperty(custName);
    }
    

    public StringProperty custNameProperty() {
        return custName;
    }
    
    /**
     * @return the custAddress
     */
    public String getCustAddress() {
        return custAddress.get();
    }

    /**
     * @param custAddress the custAddress to set
     */
    public void setCustAddress(String custAddress) {
        this.custAddress = new SimpleStringProperty(custAddress);
    }

    public StringProperty custAddressProperty() {
       return custAddress;
    }
    /**
     * @return the custAddress2
     */
    public String getCustAddress2() {
        return custAddress2.get();
    }

    /**
     * @param custAddress2 the custAddress2 to set
     */
    public void setCustAddress2(String custAddress2) {
        this.custAddress2 = new SimpleStringProperty(custAddress2);
    }

    public StringProperty custAddress2Property() {
        return custAddress2;
    }
    /**
     * @return the custCity
     */
    public String getCustCity() {
        return custCity.get();
    }

    /**
     * @param custCity the custCity to set
     */
    public void setCustCity(String custCity) {
        this.custCity = new SimpleStringProperty(custCity);
    }

    public StringProperty custCityProperty() {
        return custCity;
    }
    /**
     * @return the custZip
     */
    public String getCustZip() {
        return custZip.get();
    }

    /**
     * @param custZip the custZip to set
     */
    public void setCustZip(String custZip) {
        this.custZip = new SimpleStringProperty(custZip);
    }

    public StringProperty custZipProperty() {
        return custZip;
    }
    /**
     * @return the custCountry
     */
    public String getCustCountry() {
        return custCountry.get();
    }

    /**
     * @param custCountry the custCountry to set
     */
    public void setCustCountry(String custCountry) {
        this.custCountry = new SimpleStringProperty(custCountry);
    }

    public StringProperty custCountryProperty() {
        return custCountry;
    }
    /**
     * @return the custPhone
     */
    public String getCustPhone() {
        return custPhone.get();
    }

    /**
     * @param custPhone the custPhone to set
     */
    public void setCustPhone(String custPhone) {
        this.custPhone = new SimpleStringProperty(custPhone);
    }

    public StringProperty custPhoneProperty() {
        return custPhone;
    }

    public int getIDColumnAsInt() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
